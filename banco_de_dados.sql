-- MySQL dump 10.13  Distrib 5.5.31, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: scepoli
-- ------------------------------------------------------
-- Server version	5.5.31-0ubuntu0.13.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `scepoli`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `polijr116` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `polijr116`;

--
-- Table structure for table `acao_social`
--

DROP TABLE IF EXISTS `acao_social`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acao_social` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `olho` text,
  `texto` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acao_social`
--

LOCK TABLES `acao_social` WRITE;
/*!40000 ALTER TABLE `acao_social` DISABLE KEYS */;
INSERT INTO `acao_social` VALUES (1,'<p>A Semana de Cultura Empresarial tem um forte car&aacute;ter social. A cada edi&ccedil;&atilde;o, todo o dinheiro&nbsp;obtido com a venda da bilheteria da programa&ccedil;&atilde;o &eacute; revertido para um projeto social, em que a equipe tem&nbsp;oportunidade de n&atilde;o s&oacute; contribuir financeiramente, mas tamb&eacute;m participar ativamente das atividades da&nbsp;institui&ccedil;&atilde;o escolhida.</p>','<p>Nesse ano, a institui&ccedil;&atilde;o escolhida foi o Matem&aacute;tica em Movimento, grupo formado por<br />estudantes da POLI!</p>\n<p><img src=\"../../../_imgs/banco/logo-matematica-em-movimento_140513_131413.png\" alt=\"\" /></p>\n<p>Criado em 2012, &ldquo;Matem&aacute;tica em Movimento&rdquo; &eacute; um projeto solid&aacute;rio fundado, coordenado e executado&nbsp;por alunos da gradua&ccedil;&atilde;o da Escola Polit&eacute;cnica da USP. &Eacute; um projeto da ONG SOMAR &ndash; Solidariedade&nbsp;em Marcha, que tem a classifica&ccedil;&atilde;o de OSCIP (Organiza&ccedil;&atilde;o da Sociedade Civil de Interesse P&uacute;blico).</p>\n<p>O projeto tem como foco a juventude brasileira que n&atilde;o tem acesso a uma educa&ccedil;&atilde;o digna e de&nbsp;qualidade. Nossa miss&atilde;o consiste em contribuir de forma concreta para a forma&ccedil;&atilde;o pessoal e acad&ecirc;mica&nbsp;de alunos do ensino m&eacute;dio p&uacute;blico paulistano atrav&eacute;s de aulas de aprofundamento em matem&aacute;tica. Como&nbsp;conseq&uuml;&ecirc;ncia, a solidariedade, a multiplica&ccedil;&atilde;o do conhecimento e o contato humano s&atilde;o atitudes que&nbsp;afloram naturalmente de todos os envolvidos no projeto.</p>\n<p>Contamos hoje com alunos de desempenho exemplar que cursam o primeiro ou segundo ano do ensino&nbsp;m&eacute;dio p&uacute;blico, uma equipe de 12 professores motivados, unidos e vision&aacute;rios, al&eacute;m de toda a&nbsp;infraestrutura necess&aacute;ria para o desenvolvimento de um curso feito exclusivamente com foco nos alunos.</p>\n<p>O projeto tem a ambi&ccedil;&atilde;o de crescer e se tornar refer&ecirc;ncia no compartilhamento do conhecimento e na&nbsp;promo&ccedil;&atilde;o de mudan&ccedil;as sem esperar que algu&eacute;m fa&ccedil;a por voc&ecirc;.</p>');
/*!40000 ALTER TABLE `acao_social` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acao_social_imagens`
--

DROP TABLE IF EXISTS `acao_social_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acao_social_imagens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(140) DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `id_parent` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acao_social_imagens`
--

LOCK TABLES `acao_social_imagens` WRITE;
/*!40000 ALTER TABLE `acao_social_imagens` DISABLE KEYS */;
INSERT INTO `acao_social_imagens` VALUES (1,'foto1.jpg',0,1),(3,'foto31.jpg',2,1),(4,'foto2.jpg',1,1);
/*!40000 ALTER TABLE `acao_social_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(140) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `destino` varchar(255) DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `titulo` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (4,'banner-12.jpg','link','',0,'Venha aprender os segredos de quem chegou lá!'),(5,'banner-2.jpg','video','http://vimeo.com/42921959',1,'Apresentação em Vídeo da Semana de Cultura Empresarial');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cadastros`
--

DROP TABLE IF EXISTS `cadastros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadastros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `data_cadastro` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cadastros`
--

LOCK TABLES `cadastros` WRITE;
/*!40000 ALTER TABLE `cadastros` DISABLE KEYS */;
/*!40000 ALTER TABLE `cadastros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chamadas`
--

DROP TABLE IF EXISTS `chamadas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chamadas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(140) DEFAULT NULL,
  `titulo` varchar(140) DEFAULT NULL,
  `descricao` varchar(140) DEFAULT NULL,
  `destino` varchar(250) DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chamadas`
--

LOCK TABLES `chamadas` WRITE;
/*!40000 ALTER TABLE `chamadas` DISABLE KEYS */;
INSERT INTO `chamadas` VALUES (4,'corporate.jpg','Oficina','Everis (liderança)','#',-1),(5,'foto-antonio-moraes.jpg','Palestra','Antonio de Moraes Neto','#',-1),(6,'foto-eurico-gushi.jpg','Palestra','Eurico Gushi','#',-1);
/*!40000 ALTER TABLE `chamadas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `como_chegar`
--

DROP TABLE IF EXISTS `como_chegar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `como_chegar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gmaps` text,
  `endereco` text,
  `texto` text,
  `olho` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `como_chegar`
--

LOCK TABLES `como_chegar` WRITE;
/*!40000 ALTER TABLE `como_chegar` DISABLE KEYS */;
INSERT INTO `como_chegar` VALUES (1,'&lt;iframe width=\"425\" height=\"350\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"http://maps.google.com.br/maps?f=q&source=s_q&hl=pt-BR&geocode;=&q=Rua+Coronel+Diogo,+São+Paulo&aq=2&oq=rua+coronel+&sll=-23.682804,-46.595546&sspn=0.865251,1.584778&ie=UTF8&hq;=&hnear=R.+Cel.+Diogo+-+São+Paulo&t=m&z=14&ll=-23.578123,-46.621368&output=embed\"&gt;&lt;/iframe>','Av. Prof. Mello Moraes, 2231 – sala A0 – Edifício da Engenharia Mecânica\nCidade Universitária – São Paulo - SP – CEP: 05508-900','<p>Os locais onde ocorrer&atilde;o os eventos da 22&ordf; Semana de Cultura Empresarial s&atilde;o:</p>\n<ul>\n<li>Aguarde mais informa&ccedil;&otilde;es</li>\n</ul>','<p>A 22&ordf; Semana de Cultura Empresaral ocorrer&aacute; na Escola Polit&eacute;cnica da USP.</p>');
/*!40000 ALTER TABLE `como_chegar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `telefone` varchar(25) DEFAULT NULL,
  `fax` varchar(25) DEFAULT NULL,
  `email` varchar(140) DEFAULT NULL,
  `flickr` varchar(140) DEFAULT NULL,
  `facebook` varchar(140) DEFAULT NULL,
  `youtube` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (1,'11 3091 5797','11 3091 5477','sce-organizacao@polijr.com.br','','facebook.com/semanadecultura.empresarial','');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cronograma`
--

DROP TABLE IF EXISTS `cronograma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cronograma` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cronograma`
--

LOCK TABLES `cronograma` WRITE;
/*!40000 ALTER TABLE `cronograma` DISABLE KEYS */;
INSERT INTO `cronograma` VALUES (11,'2013-06-03'),(12,'2013-06-04'),(13,'2013-06-05'),(14,'2013-06-06'),(15,'2013-06-07');
/*!40000 ALTER TABLE `cronograma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cronograma_item`
--

DROP TABLE IF EXISTS `cronograma_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cronograma_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `horario` datetime DEFAULT NULL,
  `palestrante` int(11) NOT NULL DEFAULT '0' COMMENT 'Para o FK do palestrante:\n- 1  => Nenhum Palestrante\n  0  => A definir\n>1  => Ids de Palestrantes',
  `titulo` varchar(140) DEFAULT NULL,
  `tipo` varchar(80) DEFAULT NULL,
  `id_parent` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cronograma_item`
--

LOCK TABLES `cronograma_item` WRITE;
/*!40000 ALTER TABLE `cronograma_item` DISABLE KEYS */;
INSERT INTO `cronograma_item` VALUES (23,'2013-05-18 07:00:00',0,'Café da manhã','oficina',10),(24,'2013-05-18 08:00:00',1,'1','oficina',10),(25,'2013-05-18 10:00:00',-1,'Marketing e Negócios','oficina',10),(26,'2013-05-18 11:00:00',3,'A Definir','palestra',10),(27,'2013-05-18 12:00:00',0,'Almoço','',10),(28,'2013-05-18 13:00:00',2,'4','oficina',10),(29,'2013-05-18 14:00:00',1,'5','oficina',10),(30,'2013-06-03 11:20:00',2,'','palestra',11),(31,'2013-06-03 11:20:00',0,'Oficina: A definir','oficina',11),(32,'2013-06-03 17:00:00',0,'MESA REDONDA','',11),(33,'2013-06-04 11:20:00',1,'','palestra',12),(34,'2013-06-04 11:20:00',0,'Oficina: A definir','oficina',12),(35,'2013-06-04 17:00:00',0,'Oficina: A definir','oficina',12),(36,'2013-06-05 11:20:00',3,'','palestra',13),(37,'2013-06-05 11:20:00',0,'Oficina: Everis / Liderança','oficina',13),(38,'0000-00-00 00:00:00',0,'0','0',0),(39,'2013-06-05 17:00:00',0,'Oficina: Sain Paul / Mercado Financeiro','oficina',13),(40,'2013-06-06 11:20:00',8,'','palestra',14),(41,'2013-06-06 11:20:00',9,'Oficina: Negociação','oficina',14),(42,'2013-06-06 17:00:00',0,'Oficina: A definir','oficina',14),(43,'2013-06-07 11:20:00',-1,'Palestra: A definir','palestra',15),(44,'2013-06-07 11:20:00',10,'1','oficina',15),(45,'2013-06-07 13:30:00',0,'Processo seletivo para o Working Day','',15);
/*!40000 ALTER TABLE `cronograma_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evento`
--

DROP TABLE IF EXISTS `evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `olho` text,
  `texto` text,
  `obs` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento`
--

LOCK TABLES `evento` WRITE;
/*!40000 ALTER TABLE `evento` DISABLE KEYS */;
INSERT INTO `evento` VALUES (1,'<p>A Semana de Cultura Empresarial (SCE) foi criada, em 1992, com o intuito de aproximar<br />estudantes de gradua&ccedil;&atilde;o com o mundo empresarial.</p>','<p>Em todas as 21 edi&ccedil;&otilde;es do evento, esse contato&nbsp;tem sido feito atrav&eacute;s de palestras ministradas por nomes de grande relev&acirc;ncia e compet&ecirc;ncia, como&nbsp;Andr&eacute; Esteves (presidente do BTG Pactual), Luiz Mendon&ccedil;a (ex-vice-presidente da Braskem, atual&nbsp;presidente da Quattor), Robinson Shiba (fundador do China in Box), Marcelo Tas, Michel Levy&nbsp;(presidente da Microsoft Brasil), Ozires Silva (fundador da Embraer), entre outros, que atrav&eacute;s da&nbsp;experi&ecirc;ncia que possuem t&ecirc;m sido capazes de antecipar aos alunos o que ser&aacute; esperado deles e o que&nbsp;eles poder&atilde;o esperar desse mundo.</p>\n<p><br />A 22&ordf; Semana de Cultura Empresarial em 2013 trar&aacute; algumas novidades para atingir todo o p&uacute;blico&nbsp;universit&aacute;rio e, em especial, os polit&eacute;cnicos. Com palestras de grandes personalidades, oficinas&nbsp;ministradas por conceituadas institui&ccedil;&otilde;es e profissionais e oportunidades de Working Day, a SCE&nbsp;promete ser um sucesso e uma grande oportunidade de crescimento e aprendizado para todos os&nbsp;participantes!</p>','O evento será realizado entre os dias 03 a 07 de Junho, na Escola Politécnica, nos anfiteatros da Administração, Produção e Elétrica.');
/*!40000 ALTER TABLE `evento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evento_imagens`
--

DROP TABLE IF EXISTS `evento_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento_imagens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(140) DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `id_parent` varchar(45) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento_imagens`
--

LOCK TABLES `evento_imagens` WRITE;
/*!40000 ALTER TABLE `evento_imagens` DISABLE KEYS */;
INSERT INTO `evento_imagens` VALUES (1,'outer_space_1600x900_wallpaper_high_resolution_wallpaper_1600x1200_www.wallpaperhi_.com_.jpg',2,'1'),(2,'japan-digital-scenery.jpg',0,'1'),(3,'ironman_wallpapers_284_resize.jpg',1,'1');
/*!40000 ALTER TABLE `evento_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingressos`
--

DROP TABLE IF EXISTS `ingressos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingressos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `olho` text,
  `texto` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingressos`
--

LOCK TABLES `ingressos` WRITE;
/*!40000 ALTER TABLE `ingressos` DISABLE KEYS */;
INSERT INTO `ingressos` VALUES (2,'<p>em breve</p>','<p>A definir</p>');
/*!40000 ALTER TABLE `ingressos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oficinas`
--

DROP TABLE IF EXISTS `oficinas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oficinas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(140) DEFAULT NULL,
  `texto` text,
  `horario` varchar(20) DEFAULT NULL,
  `horario_detalhes` varchar(50) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `por` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oficinas`
--

LOCK TABLES `oficinas` WRITE;
/*!40000 ALTER TABLE `oficinas` DISABLE KEYS */;
INSERT INTO `oficinas` VALUES (1,'Planejamento Estratégico e Inovação','<p><strong>Planejamento Estrat&eacute;gico e Inova&ccedil;&atilde;o: A Teoria na Pr&aacute;tica</strong></p>\n<p>O Objetivo deste Workshop &eacute; Apresentar de Forma Pr&aacute;tica e Objetiva<br />a Facilita&ccedil;&atilde;o de um Workshop de Planejamento Estrat&eacute;gico e Inova&ccedil;&atilde;o<br />com a Metodologia IDM - Innovation Decision Mapping</p>\n<p>- O Brainstorming de Forma Produtiva;<br />- Prioriza&ccedil;&atilde;o, Integra&ccedil;&atilde;o e Combina&ccedil;&atilde;o de Ideias;<br />- Como Inovar atrav&eacute;s da Intelig&ecirc;ncia Coletiva - Equipe;<br />- Apresentar a Metodologia IDM - Innovation Decision Mapping;<br />- Habilidade e Atitude do Facilitador de Workshop de Inova&ccedil;&atilde;o;</p>','11:20','informe-se no dia do evento','2013-06-07','Eurico Gushi'),(4,'Técnicas de Negociação','<p>A oficina objetiva trazer ao participante conceitos b&aacute;sicos envolvidos em uma negocia&ccedil;&atilde;o. Ser&atilde;o apresentadas t&eacute;cnicas de negocia&ccedil;&atilde;o e qual estrat&eacute;gia deve ser utilizada em determinada situa&ccedil;&atilde;o. Ao final, haver&aacute; um exerc&iacute;cio entre os participantes procurando simular as emo&ccedil;&otilde;es e estrat&eacute;gias de uma negocia&ccedil;&atilde;o aberta e direta.</p>','11:20','informe-se no dia do evento','2013-06-06','Manabu');
/*!40000 ALTER TABLE `oficinas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organizacao`
--

DROP TABLE IF EXISTS `organizacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organizacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(140) DEFAULT NULL,
  `imagem_fundo_branco` varchar(140) DEFAULT NULL,
  `imagem_fundo_transparente` varchar(140) DEFAULT NULL,
  `categoria` varchar(45) DEFAULT NULL,
  `texto` text,
  `ordem` int(11) DEFAULT '-1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organizacao`
--

LOCK TABLES `organizacao` WRITE;
/*!40000 ALTER TABLE `organizacao` DISABLE KEYS */;
INSERT INTO `organizacao` VALUES (1,'Bain & Company','bain-logo2.png','logo-bain-rodape.png','patrocinio','<p>A Bain &amp; Company, empresa l&iacute;der global em consultoria de neg&oacute;cios, orienta clientes em rela&ccedil;&atilde;o a estrat&eacute;gias, opera&ccedil;&otilde;es, tecnologia, constitui&ccedil;&atilde;o de empresas, fus&otilde;es e aquisi&ccedil;&otilde;es, desenvolvendo pr&aacute;ticas que assegurem aos clientes transpar&ecirc;ncia nos processos de mudan&ccedil;a e tomada de decis&otilde;es. A Consultoria trabalha em sinergia com os clientes, vinculando seu fee aos resultados. O desempenho dos clientes da Bain superou o mercado de a&ccedil;&otilde;es em 4 para 1. Fundada em 1973, em Boston, a Bain conta com 49 escrit&oacute;rios em 31 pa&iacute;ses e j&aacute; trabalhou com mais de 4.600 empresas entre multinacionais e companhias privadas e p&uacute;blicas em todos os setores da economia. Para mais informa&ccedil;&otilde;es, acesse: www.bain.com.br. Twitter: @BainAlerts.</p>\n<p>Est&aacute; presente no Brasil desde 1997 com escrit&oacute;rios em S&atilde;o Paulo e no Rio de Janeiro e atende toda a Am&eacute;rica do Sul de forma integrada com o escrit&oacute;rio de Buenos Aires.</p>\n<p>Os escrit&oacute;rios da Bain Brasil est&atilde;o entre os que mais crescem no sistema, contando atualmente com mais de 250 consultores. Desde seu in&iacute;cio no Brasil, a Bain &amp; Company trabalha com l&iacute;deres orientados a mudan&ccedil;a em mais de 800 projetos em 15 ind&uacute;strias diferentes. Nossas pr&aacute;ticas se estendem &agrave;s &aacute;reas de estrat&eacute;gia, melhoria de performance, organiza&ccedil;&atilde;o, fus&otilde;es e aquisi&ccedil;&otilde;es, private equity e TI.</p>',0),(2,'Everis','everis-logo2.png','logo-everis-rodape.png','patrocinio','<p>A Everis &eacute; uma das maiores empresas de consultoria de neg&oacute;cios, tecnologia e outsourcing do&nbsp;mundo, que faz da troca de experi&ecirc;ncia global uma vantagem para clientes e funcion&aacute;rios.&nbsp;Presente na Europa, Am&eacute;rica Latina e Estados Unidos, a Everis possui um formato diferenciado&nbsp;de trabalho, que valoriza o interc&acirc;mbio constante e sem fronteiras de conhecimento, informa&ccedil;&atilde;o e&nbsp;pessoas. Atuamos em grandes companhias e em diversos setores da economia, desde a estrat&eacute;gia de&nbsp;neg&oacute;cio at&eacute; a implanta&ccedil;&atilde;o e evolu&ccedil;&atilde;o de cada projeto.</p>\n<p>&nbsp;</p>\n<p>Nosso principal ativo s&atilde;o as pessoas, portanto desde o primeiro dia de trabalho, designamos um&nbsp;mentor experiente para acompanhar e direcionar o crescimento do profissional. Al&eacute;m de proporcionar&nbsp;uma forma&ccedil;&atilde;o cont&iacute;nua com, por exemplo, cursos em nossa Universidade Corporativa.</p>\n<p>&nbsp;</p>\n<p>Se voc&ecirc; quer compartilhar suas ideias e crescer profissionalmente, a Everis &eacute; seu lugar.</p>',2),(3,'Poli Jr','poli-logo2.png','logo-poli-rodape1.png','realizacao','<p>Fundada em agosto de 1989, a Poli J&uacute;nior &eacute; uma institui&ccedil;&atilde;o sem fins lucrativos, constitu&iacute;da e<br />gerida exclusivamente por alunos da Escola Polit&eacute;cnica da USP. Tem experi&ecirc;ncia de mais de 23 anos na&nbsp;presta&ccedil;&atilde;o de servi&ccedil;os com foco em pequenos empres&aacute;rios e pessoas f&iacute;sicas interessadas em come&ccedil;ar&nbsp;um novo neg&oacute;cio.</p>\n<p>Sua prioridade &eacute; atuar em conjunto com institui&ccedil;&otilde;es e pessoas que possuam recursos limitados,&nbsp;mas tem tempo e disponibilidade para trabalhar em parceria com universit&aacute;rios interessados em se&nbsp;desenvolver atrav&eacute;s da presta&ccedil;&atilde;o de servi&ccedil;os e projetos de engenharia.</p>\n<p>Tem uma equipe de 90 universit&aacute;rios das diversas &aacute;reas da engenharia que a Escola polit&eacute;cnica&nbsp;da USP ministra, dispostos a buscar informa&ccedil;&otilde;es com professores e bibliografias conceituadas para a&nbsp;constru&ccedil;&atilde;o de solu&ccedil;&otilde;es inovadores e de grande impacto para o seu neg&oacute;cio.</p>',-1),(4,'Fundação Vanzolini','vanzolini-logo1.png','logo-vanzolini-rodape1.png','apoio','<p>A Funda&ccedil;&atilde;o Vanzolini &eacute; uma institui&ccedil;&atilde;o privada, sem fins lucrativos, criada, mantida e gerida<br />pelos professores do Departamento de Engenharia de Produ&ccedil;&atilde;o da Escola Polit&eacute;cnica da Universidade&nbsp;de S&atilde;o Paulo. Tem como objetivo desenvolver e disseminar conhecimentos cient&iacute;ficos e tecnol&oacute;gicos&nbsp;inerentes &agrave; Engenharia de Produ&ccedil;&atilde;o, &agrave; Administra&ccedil;&atilde;o Industrial, &agrave; Gest&atilde;o de Opera&ccedil;&otilde;es e &agrave;s demais&nbsp;atividades correlatas que realiza, com total car&aacute;ter inovador.</p>\n<p><br />Tamb&eacute;m prioriza seus projetos e atividades de Educa&ccedil;&atilde;o Continuada por relev&acirc;ncia econ&ocirc;mica&nbsp;e social e, por esse motivo, pauta sua atua&ccedil;&atilde;o por crit&eacute;rios de excel&ecirc;ncia acad&ecirc;micos, profissionais e&nbsp;&eacute;ticos. A Funda&ccedil;&atilde;o Vanzolini &eacute; ainda um centro de refer&ecirc;ncia internacional em temas de destaque para&nbsp;as empresas privadas e para os &oacute;rg&atilde;os e entidades do setor p&uacute;blico que buscam alcan&ccedil;ar e manter&nbsp;padr&otilde;es elevados de desempenho.</p>\n<p>&nbsp;</p>\n<p><a title=\"Fundação Vanzolini\" href=\"http://www.vanzolini.org.br/\" target=\"_blank\">http://www.vanzolini.org.br/</a></p>',2),(5,'Associação dos Engenheiros Politécnicos','engenheiros-logo2.png','logo-engenheiros-rodape.png','apoio','<p>Associa&ccedil;&atilde;o que busca integrar e agregar valor aos membros da comunidade polit&eacute;cnica: alunos, ex&mdash;alunos, professores e empresas. Para tanto, um dos programas desenvolvidos pela AEP &eacute; o Programa Profissional do Futuro que visa complementar a forma&ccedil;&atilde;o dos alunos e ex-alunos em termos de autoconhecimento, gest&atilde;o de carreira, curso de capacita&ccedil;&atilde;o em neg&oacute;cios, orienta&ccedil;&atilde;o e desenvolvimento de compet&ecirc;ncias profissionais, mercado de trabalho, etc.</p>\n<p><a title=\"Associação dos Engenheiros Politécnicos\" href=\"http://sites.poli.usp.br/org/aep/\" target=\"_blank\">http://sites.poli.usp.br/org/aep/</a></p>',0),(6,'Endowment da Escola Politécnica','endowment-logo2.png','logo-endowment-rodape.png','apoio','',1),(7,'Anglo American','anglo-logo2.png','logo-anglo-rodape.png','patrocinio','',1);
/*!40000 ALTER TABLE `organizacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `palestrante_texto`
--

DROP TABLE IF EXISTS `palestrante_texto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `palestrante_texto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `palestrante_texto`
--

LOCK TABLES `palestrante_texto` WRITE;
/*!40000 ALTER TABLE `palestrante_texto` DISABLE KEYS */;
INSERT INTO `palestrante_texto` VALUES (1,'<p>As palestras realizadas pela Semana de Cultura Empresarial de destacam por serem motivadoras, mostrando ao alunos que eles podem ser profissionais melhores. H&aacute; 22 anos, a SCE busca palestrantes que possam desempenhar este papel no evento.</p>');
/*!40000 ALTER TABLE `palestrante_texto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `palestrantes`
--

DROP TABLE IF EXISTS `palestrantes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `palestrantes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(140) DEFAULT NULL,
  `imagem` varchar(140) DEFAULT NULL,
  `texto` text,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `palestrantes`
--

LOCK TABLES `palestrantes` WRITE;
/*!40000 ALTER TABLE `palestrantes` DISABLE KEYS */;
INSERT INTO `palestrantes` VALUES (1,'Antonio de Moraes Neto','antonio_moraes_neto.jpg','<p>Mus sed odio elit proin eu, mus purus? Mattis natoque cum magnis, auctor ridiculus tempor, hac aenean adipiscing. Sed augue, vut sociis scelerisque, montes, cras, diam urna augue ut adipiscing aenean pid tortor enim enim augue. Mus tortor dictumst placerat, lundium nec, risus pid, natoque! Ultrices magna magna, vel pulvinar.</p>\n<p>Sit, cras et dolor turpis lacus elementum et ridiculus ac nunc in placerat vel lacus? Vut! Ridiculus. Pid? Duis, porttitor, cum. Placerat porttitor in facilisis rhoncus nisi platea lacus odio est nunc diam, diam parturient dolor dolor! Augue? In dis, ac et nisi vel tincidunt, sit pellentesque lacus, lorem.</p>',0),(2,'Ozires Silva','perfil-vazio8.jpg','<p>Mus sed odio elit proin eu, mus purus? Mattis natoque cum magnis, auctor ridiculus tempor, hac aenean adipiscing. Sed augue, vut sociis scelerisque, montes, cras, diam urna augue ut adipiscing aenean pid tortor enim enim augue. Mus tortor dictumst placerat, lundium nec, risus pid, natoque! Ultrices magna magna, vel pulvinar.</p>\n<p>Sit, cras et dolor turpis lacus elementum et ridiculus ac nunc in placerat vel lacus? Vut! Ridiculus. Pid? Duis, porttitor, cum. Placerat porttitor in facilisis rhoncus nisi platea lacus odio est nunc diam, diam parturient dolor dolor! Augue? In dis, ac et nisi vel tincidunt, sit pellentesque lacus, lorem.</p>',1),(3,'Robinson Shiba','02.jpg','<p>Mus sed odio elit proin eu, mus purus? Mattis natoque cum magnis, auctor ridiculus tempor, hac aenean adipiscing. Sed augue, vut sociis scelerisque, montes, cras, diam urna augue ut adipiscing aenean pid tortor enim enim augue. Mus tortor dictumst placerat, lundium nec, risus pid, natoque! Ultrices magna magna, vel pulvinar.</p>\n<p>Sit, cras et dolor turpis lacus elementum et ridiculus ac nunc in placerat vel lacus? Vut! Ridiculus. Pid? Duis, porttitor, cum. Placerat porttitor in facilisis rhoncus nisi platea lacus odio est nunc diam, diam parturient dolor dolor! Augue? In dis, ac et nisi vel tincidunt, sit pellentesque lacus, lorem.</p>',2),(8,'Amilcare Dallevo Jr.','perfil-vazio5.jpg','<p>a definir</p>',-1),(9,'Marcelo Park','perfil-vazio6.jpg','<p>Oficina de Negocia&ccedil;&atilde;o</p>',-1),(10,'Eurico Gushi','imagem1.jpg','<p>Oficina de Planejamento Estrat&eacute;gico</p>',-1),(11,'Saint Paul','perfil-vazio7.jpg','<p>Oficina: Mercado Financeiro</p>',-1),(12,'Manabu','fotomanabu.jpg','<p>Oficina: T&eacute;cnicas de Negocia&ccedil;&atilde;o.</p>',-1);
/*!40000 ALTER TABLE `palestrantes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password` varchar(140) NOT NULL,
  `email` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `NomeUsuario` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (11,'trupe','d32c9694c72f1799ec545c82c8309c02','contato@trupe.net');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-05-17 10:29:27
