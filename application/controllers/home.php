<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
   		$this->load->model('banners_model', 'banners');
   		$this->load->model('chamadas_model', 'chamadas');
    }

    function index(){
    	$data['banners'] = $this->banners->pegarTodos();
   		$data['chamadas'] = $this->chamadas->pegarTodos();
   		$this->load->view('home', $data);
    }

}