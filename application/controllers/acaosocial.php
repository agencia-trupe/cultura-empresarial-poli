<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Acaosocial extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
    	$this->load->model('acao_social_model', 'acaosocial');
    	$data['acaosocial'] = $this->acaosocial->pegarTodos();
    	$data['acaosocial'][0]->imagens = $this->acaosocial->imagens($data['acaosocial'][0]->id);
   		$this->load->view('acao-social', $data);
    }

}