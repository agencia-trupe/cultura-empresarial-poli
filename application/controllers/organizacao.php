<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Organizacao extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
    	$data['organizacao']['realizacao'] = $this->db->order_by('ordem', 'asc')->get_where('organizacao', array('categoria'=>'realizacao'))->result();
        $data['organizacao']['patrocinio'] = $this->db->order_by('ordem', 'asc')->get_where('organizacao', array('categoria'=>'patrocinio'))->result();
        $data['organizacao']['apoio'] = $this->db->order_by('ordem', 'asc')->get_where('organizacao', array('categoria'=>'apoio'))->result();

    	$this->load->view('organizacao', $data);
    }

    function detalhes($id = false){
        $this->hasLayout = FALSE;
        if($id){
            $this->load->model('organizacao_model', 'organizacao');
            $data['detalhes'] = $this->organizacao->pegarPorId($id);
            if(($data['detalhes']))
                echo $this->load->view('organizacao-detalhes', $data, TRUE);
        }   
    }

}