<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comochegar extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
    	$data['comochegar'] = $this->comochegar->pegarTodos();
   		$this->load->view('como-chegar', $data);
    }

}