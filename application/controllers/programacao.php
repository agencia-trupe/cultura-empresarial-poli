<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Programacao extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();        
    }

    function index(){
   		redirect('programacao/cronograma');
    }

    function cronograma(){
        $this->load->model('cronograma_model', 'cronograma');

        $data['dias'] = $this->cronograma->pegarTodos();
        foreach ($data['dias'] as $key => $value) {
            $value->atividades = $this->cronograma->atividades($value->id);
        }

    	$this->load->view('programacao/cronograma', $data);
    }

    function oficinas(){
        $this->load->model('oficinas_model', 'oficinas');

        $data['oficinas'] = $this->oficinas->pegarTodos();

        foreach ($data['oficinas'] as $key => $value) {
            $value->vagas = $this->oficinas->vagas($value->id);
        }

    	$this->load->view('programacao/oficinas', $data);	
    }

    function palestrantes(){
        $this->load->model('palestrantes_model', 'palestrantes');

        $data['palestrantes'] = $this->palestrantes->pegarTodos();
        $data['texto'] = $this->palestrantes->pegarTexto();

    	$this->load->view('programacao/palestrantes', $data);
    }

    function perfil($id = false){
        $this->hasLayout = FALSE;
        if($id){
            $this->load->model('palestrantes_model', 'palestrantes');
            $data['detalhes'] = $this->palestrantes->pegarPorId($id);
            if(($data['detalhes']))
                echo $this->load->view('programacao/perfil', $data, TRUE);
        }
    }

    function veroficina($id = false){
        $this->hasLayout = FALSE;
        if($id){
            $this->load->model('oficinas_model', 'oficinas');
            $data['detalhes'] = $this->oficinas->pegarPorId($id);
            $data['detalhes']->vagas = $this->oficinas->vagas($data['detalhes']->id);
            if(($data['detalhes']))
                echo $this->load->view('programacao/oficinas-detalhes', $data, TRUE);
        }   
    }

    function verminicurso($id = false){
        $this->hasLayout = FALSE;
        if($id){
            $this->load->model('minicursos_model', 'minicursos');
            $data['detalhes'] = $this->minicursos->pegarPorId($id);
            $data['detalhes']->vagas = $this->minicursos->vagas($data['detalhes']->id);
            if(($data['detalhes']))
                echo $this->load->view('programacao/minicursos-detalhes', $data, TRUE);
        }   
    }    

    function minicursos(){
        $this->load->model('minicursos_model', 'minicursos');

        $data['minicursos'] = $this->minicursos->pegarTodos();

        foreach ($data['minicursos'] as $key => $value) {
            $value->vagas = $this->minicursos->vagas($value->id);
        }

        $this->load->view('programacao/minicursos', $data);
    }

    function inscricaoMinicursos($id_curso = false){
        $this->load->model('minicursos_model', 'minicursos');

        if(!$id_curso) redirect('programacao/minicursos');

        $data['cursoSel'] = '';
        $data['vagasSel'] = '0000';

        if($_POST){

            $retorno = $this->minicursos->inscrever();

            if($retorno['status'] === false){

                $this->session->set_flashdata('mini_cursos_id', $this->input->post('mini_cursos_id'));
                $this->session->set_flashdata('nome', $this->input->post('nome'));
                $this->session->set_flashdata('celular', $this->input->post('celular'));
                $this->session->set_flashdata('faculdade', $this->input->post('faculdade'));
                $this->session->set_flashdata('curso', $this->input->post('curso'));
                $this->session->set_flashdata('anoIngresso', $this->input->post('anoIngresso'));
                $this->session->set_flashdata('email', $this->input->post('email'));
                $this->session->set_flashdata('confirmeEmail', $this->input->post('confirmeEmail'));

                $this->session->set_flashdata('mensagem', $retorno['mensagem']);

                redirect('programacao/inscricao-minicursos/'.$this->input->post('mini_cursos_id'));

            }else{
                $data['inscricao'] = $retorno['dados'];
                $data['back'] = 'minicursos';
                $this->load->view('programacao/posInscricao', $data);
            }

        }else{
            $data['cursoSel'] = $id_curso;
            $data['minicursos'] = $this->minicursos->pegarTodos();

            foreach ($data['minicursos'] as $key => $value) {
                $value->vagas = $this->minicursos->vagas($value->id);
                if($value->id == $id_curso) $data['vagasSel'] = str_pad($this->minicursos->vagas($value->id), 4, '0', STR_PAD_LEFT);
            }

            $this->load->view('programacao/inscricao', $data);            
        }        
    }

    function inscricaoOficinas($id_curso = false){
        $this->load->model('oficinas_model', 'oficinas');

        if(!$id_curso) redirect('programacao/oficinas');

        $data['cursoSel'] = '';
        $data['vagasSel'] = '0000';

        if($_POST){

            $retorno = $this->oficinas->inscrever();

            if($retorno['status'] === false){

                $this->session->set_flashdata('oficinas_id', $this->input->post('oficinas_id'));
                $this->session->set_flashdata('nome', $this->input->post('nome'));
                $this->session->set_flashdata('celular', $this->input->post('celular'));
                $this->session->set_flashdata('faculdade', $this->input->post('faculdade'));
                $this->session->set_flashdata('curso', $this->input->post('curso'));
                $this->session->set_flashdata('anoIngresso', $this->input->post('anoIngresso'));
                $this->session->set_flashdata('email', $this->input->post('email'));
                $this->session->set_flashdata('confirmeEmail', $this->input->post('confirmeEmail'));

                $this->session->set_flashdata('mensagem', $retorno['mensagem']);

                redirect('programacao/inscricao-oficinas/'.$this->input->post('oficinas_id'));

            }else{
                $data['inscricao'] = $retorno['dados'];
                $data['back'] = 'oficinas';
                $this->load->view('programacao/posInscricao', $data);
            }

        }else{
            $data['cursoSel'] = $id_curso;
            $data['oficinas'] = $this->oficinas->pegarTodos();

            foreach ($data['oficinas'] as $key => $value) {
                $value->vagas = $this->oficinas->vagas($value->id);
                if($value->id == $id_curso) $data['vagasSel'] = str_pad($this->oficinas->vagas($value->id), 4, '0', STR_PAD_LEFT);
            }

            $this->load->view('programacao/inscricao-oficinas', $data);            
        }        
    }    
}