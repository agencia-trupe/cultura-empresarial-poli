<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
    	$data['contato'] = $this->contato->pegarTodos();
    	$data['como_chegar'] = $this->comochegar->pegarTodos();
   		$this->load->view('contato', $data);
    }

	function enviar(){

		$data['contato'] = $this->contato->pegarTodos();

	    $nome = $this->input->post('nome');
	    $email = $this->input->post('email');
	    $telefone = $this->input->post('telefone');
	    $mensagem = $this->input->post('mensagem');

	    if($nome && $email && $mensagem){
	        $emailconf['charset'] = 'utf-8';
	        $emailconf['mailtype'] = 'html';
	        //$emailconf['protocol'] = 'smtp';
	        //$emailconf['smtp_host'] = 'smtp.polijr.com.br';
	        //$emailconf['smtp_user'] = 'noreply@polijr.com.br';
	        //$emailconf['smtp_pass'] = 'no762PoJrply';

	        $this->load->library('email');

	        $this->email->initialize($emailconf);
	        
	        $from = 'noreply@polijr.com.br';
	        $fromname = 'SCE Poli Jr';
	        $to = $data['contato'][0]->email;
	        $bcc = 'bruno@trupe.net';
	        $assunto = 'Contato via Site';
	        
	        
	        $email = <<<EML
<!DOCTYPE html>
<html>
<head>
    <title>Mensagem de contato via formulário do site</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Nome :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$nome</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>E-mail :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$email</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Telefone :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$telefone</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Mensagem :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$mensagem</span>
</body>
</html>
EML;

	        $plain = <<<EML
Nome :$nome\r\n
E-mail :$email\r\n
Telefone :$telefone\r\n
Mensagem :$mensagem
EML;

	        $this->email->from($from, $fromname);
	        $this->email->to($to);
	        if($bcc)
	            $this->email->bcc($bcc);
	        $this->email->reply_to($email);

	        $this->email->subject($assunto);
	        $this->email->message($email);
	        $this->email->set_alt_message($plain);

	        if(!$this->email->send())
	        	die($this->email->print_debugger());
	    }

		$this->session->set_flashdata('contato_ok', true);
		redirect('contato/index');
	}

}