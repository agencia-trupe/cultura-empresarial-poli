<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Minicursos extends MY_Admincontroller {

    function __construct(){
        parent::__construct();

        $this->titulo = "Mini Cursos";
        $this->unidade = "Curso";
        $this->load->model('minicursos_model', 'model');
    }

    function index($pag = 0){

        $data['registros'] = $this->model->pegarTodos('ordem', 'ASC');

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;
         
        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
        else
            $data['mostrarsucesso'] = false;

        foreach ($data['registros'] as $key => $value) {
            $value->n_inscricoes = $this->model->n_inscricoes($value->id);
        }

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

    function inscricoes($id_curso, $id_inscricao = false){
        if($id_inscricao){
            $data['parent'] = $this->model->pegarPorId($id_curso);
            $data['inscricoes'] = $this->model->inscricoes($id_curso);
            $data['registro'] = $this->model->inscricoes($id_curso, $id_inscricao);
            if(!$data['parent'])
                redirect('painel/'.$this->router->class);
        }else{
            $data['parent'] = $this->model->pegarPorId($id_curso);
            $data['inscricoes'] = $this->model->inscricoes($id_curso);
            $data['registro'] = FALSE;
        }

        if(isset($data['parent']->titulo))
            $titulo_atual = $data['parent']->titulo;
        elseif(isset($data['parent']->nome))
            $titulo_atual = $data['parent']->nome;
        else
            $titulo_atual = $this->titulo;

        $data['parent']->inscritos = $this->model->n_inscricoes($data['parent']->id);
        
        $data['campo_1'] = "Inscrição";
        $data['titulo'] = 'Inscrições no Mini Curso : '.$titulo_atual;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/inscricoes', $data);
    }

    function excluirInscricao($id_inscricao, $id_curso){
        if($this->model->excluirInscricao($id_inscricao)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Inscrição excluída com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir Inscrição');
        }

        redirect('painel/'.$this->router->class.'/inscricoes/'.$id_curso, 'refresh');
    }

    function verInscricao($id_inscricao, $id_curso){
        $data['curso'] = $this->model->pegarPorId($id_curso);
        $data['registro'] = $this->model->inscricoes($id_curso, $id_inscricao);
        $this->hasLayout = false;
        $this->load->view('painel/'.$this->router->class.'/detalhesInscricao', $data);
    }

    function download($id_curso){
        $this->hasLayout = FALSE;
        $this->load->dbutil();

        $str_query = <<<STR
SELECT
curso.titulo as 'Título do Curso',
cad.nome as Nome,
cad.celular as Celular,
cad.faculdade as Faculdade,
cad.curso as Curso,
cad.ano_ingresso as 'Ano de Ingresso',
cad.email as 'E-mail',
DATE_FORMAT( cad.data_inscricao, '%d/%m/%Y' )  as 'Data de Inscrição',
cad.ip_inscricao as 'IP de Cadastro'
FROM mini_cursos_inscricoes as cad
LEFT JOIN mini_cursos as curso ON cad.mini_cursos_id = curso.id
WHERE cad.mini_cursos_id = '{$id_curso}'
ORDER BY cad.nome ASC
STR;

        $users = $this->db->query($str_query);
        $filename = "inscricoes_".Date('d-m-Y_H-i-s').'.csv';

        $delimiter = ";";
        $newline = "\r\n";

        $this->output->set_header('Content-Type: application/force-download');
        $this->output->set_header("Content-Disposition: attachment; filename='$filename'");
        $this->output->set_content_type('text/csv')->set_output(utf8_decode($this->dbutil->csv_from_result($users, $delimiter, $newline)), "ISO-8859-1", "UTF-8");
    }
}