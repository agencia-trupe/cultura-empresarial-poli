<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Como_chegar extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Como Chegar";
		$this->unidade = "Informações";
		$this->load->model('como_chegar_model', 'model');
	}

}