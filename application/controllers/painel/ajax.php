<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

   function __construct(){
   		parent::__construct();

   		if(!$this->input->is_ajax_request())
   			redirect('painel/home');
   }

	function gravaOrdem(){
        $menu = $this->input->post('data');
        for ($i = 0; $i < count($menu); $i++) {
            $this->db->set('ordem', $i)
            		 ->where('id', $menu[$i])
            		 ->update($this->input->post('tabela'));
        }
	} 

  function buscaInscricao(){
    $this->load->model('minicursos_model', 'modelminicursos');
    $this->load->model('oficinas_model', 'modeloficinas');
    
    $termo = $this->input->post('termo');
    $tipo = $this->input->post('tipo');

    $campo = ($tipo == 'mini_cursos') ? 'mini_cursos_id' : 'oficinas_id';

    $resultados = $this->db->like('nome', $termo)->or_like('email', $termo)->get($tipo.'_inscricoes')->result();
    foreach ($resultados as $value) {
      $curso = ($tipo == 'mini_cursos') ? $this->modelminicursos->pegarPorId($value->mini_cursos_id) : $this->modeloficinas->pegarPorId($value->oficinas_id);
      $value->nomeCurso = (isset($curso) && isset($curso->titulo) && $curso->titulo) ? $curso->titulo : 'Curso não encontrado';      
    }

    echo json_encode($resultados);
  }

}
