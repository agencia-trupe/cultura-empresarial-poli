<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Organizacao extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Organização";
		$this->unidade = "Instituição";
		$this->load->model('organizacao_model', 'model');
	}

    function index($tipo = 'todos'){

        $data['registros'] = $this->model->pegarTodos('ordem', 'ASC', $tipo);

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;
         
        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
        else
            $data['mostrarsucesso'] = false;

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $data['ordenacao'] = $tipo;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

}