<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Palestrantes extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Palestrantes";
		$this->unidade = "Palestrante";
		$this->load->model('palestrantes_model', 'model');
	}

    function index($pag = 0){

        $data['registros'] = $this->model->pegarTodos('ordem', 'ASC');

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;
         
        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
        else
            $data['mostrarsucesso'] = false;

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

	function formTexto(){
        $data['registro'] = $this->model->pegarTexto();
        $this->load->view('painel/'.$this->router->class.'/form-texto', $data);
	}

	function alterarTexto($id){
		if($this->model->alterarTexto($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Texto da seção alterado com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar Texto da seção');
        }

		redirect('painel/palestrantes', 'refresh');
	}

}