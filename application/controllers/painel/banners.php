<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banners extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Banners";
		$this->unidade = "Banner";
		$this->load->model('banners_model', 'model');
	}

    function index($pag = 0){

        $data['registros'] = $this->model->pegarTodos('ordem', 'ASC');

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;
         
        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
        else
            $data['mostrarsucesso'] = false;

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

    function inserir(){
        if($this->db->get('banners')->num_rows() < 3){
            if($this->model->inserir()){
                $this->session->set_flashdata('mostrarsucesso', true);
                $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' inserido com sucesso');
            }else{
                $this->session->set_flashdata('mostrarerro', true);
                $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir '.$this->unidade);
            }
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'O número máximo de Banners é 3');
        }

        redirect('painel/'.$this->router->class.'/index', 'refresh');
    }
}