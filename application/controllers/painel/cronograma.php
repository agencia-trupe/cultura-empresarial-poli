<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cronograma extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Cronograma";
		$this->unidade = "Data";
		$this->load->model('cronograma_model', 'model');
	}

    function atualizarTabela(){
        $itens = $this->db->get('cronograma_item')->result();
        $erros = 0;

        $this->db->query("DELETE FROM cronograma_item_novo");

        foreach ($itens as $key => $value) {

            $this->db->set('horario', $value->horario)->set('tipo', $value->tipo)->set('id_parent', $value->id_parent);

            if($value->palestrante >= 1 && ((int)$value->palestrante == $value->palestrante)){
                // Oficina cadastrada
                $this->db->set('palestrante_tipo', 'cadastrado')->set('palestrante_detalhe', $value->palestrante);
            }elseif(!is_numeric($value->palestrante)){
                // Texto Livre
                $this->db->set('palestrante_tipo', 'texto_livre')->set('palestrante_detalhe', $value->palestrante);
            }elseif($value->palestrante == -1){
                // A definir
                $this->db->set('palestrante_tipo', 'a_definir')->set('palestrante_detalhe', 'A Definir');
            }elseif($value->palestrante == 0){
                // Nao possui palestrante
                $this->db->set('palestrante_tipo', 'sem_palestrante')->set('palestrante_detalhe', '');
            }

            if(is_numeric($value->titulo) && is_int(($value->titulo + 0))){
                // Oficina cadastrada
                $this->db->set('titulo_tipo', 'oficina')->set('titulo_detalhe', $value->titulo);
            }elseif(!is_numeric($value->titulo)){
                // Texto Livre
                $this->db->set('titulo_tipo', 'texto_livre')->set('titulo_detalhe', $value->titulo);
            }
            // Opção de mini curso não entra pq não tem no atual. incluir depois

            if($this->db->insert('cronograma_item_novo')){
                echo "<div style='background-color:green'>Registro importado com sucesso<br>";
                echo "<pre>";
                print_r($value);
                echo "</pre></div>";
            }else{
                echo "<div style='background-color:red'>Erro ao importar Registro<br>";
                echo "<pre>";
                print_r($value);
                echo "</pre></div>";
                $erros++;
            }
        }

        if($erros == 0){
            $this->db->query("DROP TABLE cronograma_item");
            $this->db->query("ALTER TABLE cronograma_item_novo RENAME TO cronograma_item");
            echo "Tabela cronograma_item substituida.";
        }
    }

    function inserir(){
        if($this->model->inserir()){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' inserida com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Data já cadastrada');
        }

        redirect('painel/'.$this->router->class.'/index', 'refresh');
    }

    function atividades($id_parent, $id_atividade =  FALSE){
        if($id_atividade){
            $data['registro'] = $this->model->atividades($id_parent, $id_atividade);
            if(!$data['parent'])
                redirect('painel/'.$this->router->class);
        }else{
            $data['registro'] = FALSE;
        }

        $data['parent'] = $this->model->pegarPorId($id_parent);
        $data['atividades'] = $this->model->atividades($id_parent);

        $data['titulo'] = 'Atividades do dia : '.date('d/m/Y', strtotime($data['parent']->data)).' - '.diaDaSemana(date('w', strtotime($data['parent']->data)));
        $data['unidade'] = "Atividade";
        $this->load->view('painel/'.$this->router->class.'/atividades', $data);
    }

    function formAtividades($id_parent, $id = false){
        if($id){
            $data['registro'] = $this->model->pegarAtividadePorId($id);
            $data['titulo'] = "Alterar Atividade";
            if(!$data['registro'])
                redirect('painel/'.$this->router->class);
        }else{
            $data['registro'] = FALSE;
            $data['titulo'] = "Adicionar Atividade";
        }

        $data['parent'] = $this->model->pegarPorId($id_parent);
        $data['titulo_parent'] = 'Atividades do dia : '.date('d/m/Y', strtotime($data['parent']->data)).' - '.diaDaSemana(date('w', strtotime($data['parent']->data)));
        $data['palestrantes'] = $this->db->order_by('ordem', 'asc')->get('palestrantes')->result();
        $data['oficinas'] = $this->db->order_by('data', 'desc')->get('oficinas')->result();
        $data['minicursos'] = $this->db->order_by('data', 'desc')->get('mini_cursos')->result();
        $data['unidade'] = "Atividade";
        $this->load->view('painel/'.$this->router->class.'/form-atividades', $data);
    }

    function inserirAtividade(){
        if($this->model->inserirAtividade()){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Atividade inserida com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir Atividade');
        }

        redirect('painel/'.$this->router->class.'/atividades/'.$this->input->post('parent_id'), 'refresh');
    }

    function editarAtividade($id_atividade){
        if($this->model->editarAtividade($id_atividade)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Atividade alterada com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar Atividade');
        }

        redirect('painel/'.$this->router->class.'/atividades/'.$this->input->post('parent_id'), 'refresh');
    }

    function excluirAtividade($id_atividade, $id_data){
        if($this->model->excluirAtividade($id_atividade)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Atividade excluida com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir Atividade');
        }

        redirect('painel/'.$this->router->class.'/atividades/'.$id_data, 'refresh');
    }

}