<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends MY_Admincontroller {

   function __construct(){
   	parent::__construct();

   	$this->load->model('blog_model', 'model');
   }

   function index($pag = 0){
      $this->load->library('pagination');

      $pag_options = array(
         'base_url' => base_url("painel/".$this->router->class."/index/"),
         'per_page' => 15,
         'uri_segment' => 4,
         'next_link' => "Próxima →",
         'next_tag_open' => "<li class='next'>",
         'next_tag_close' => '</li>',
         'prev_link' => "← Anterior",
         'prev_tag_open' => "<li class='prev'>",
         'prev_tag_close' => '</li>',
         'display_pages' => TRUE,
         'num_links' => 10,
         'first_link' => FALSE,
         'last_link' => FALSE,
         'num_tag_open' => '<li>',
         'num_tag_close' => '</li>',
         'cur_tag_open' => '<li><b>',
         'cur_tag_close' => '</b></li>',
         'total_rows' => $this->model->numeroResultados()
      );

      $this->pagination->initialize($pag_options);
      $data['paginacao'] = $this->pagination->create_links();

   	$data['registros'] = $this->model->pegarPaginado($pag_options['per_page'], $pag, 'data', 'desc');

      $data['titulo'] = 'Posts do Blog';
      $data['unidade'] = "Post";

      if($this->session->flashdata('mostrarerro') === true)
         $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
      else
         $data['mostrarerro'] = false;

      if($this->session->flashdata('mostrarsucesso') === true)
         $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
      else
         $data['mostrarsucesso'] = false;      
   
      foreach ($data['registros'] as $key => $value) {
         $qry = $this->model->categorias($value->id_blog_categorias);
         if($qry)
            $value->categoria = $qry->titulo;
         else
            $value->categoria = 'Categoria Não Encontrada';
      }

      $this->load->view('painel/blog/lista', $data);
   }

   function form($id = false){
      if($id){
   	   $data['registro'] = $this->model->pegarPorId($id);
         if(!$data['registro'])
            redirect('painel/blog');
      }else{
         $data['registro'] = FALSE;
      }

      $data['titulo'] = 'Blog';
      $data['unidade'] = "Post";
      $data['categorias'] = $this->model->categorias();
   	$this->load->view('painel/blog/form', $data);
   }

   function inserir(){
      if($this->model->inserir()){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Post inserido com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir Post');
      }

   	redirect('painel/blog/index', 'refresh');
   }

   function alterar($id){
      if($this->model->alterar($id)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Post alterado com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar Post');
      }     
   	redirect('painel/blog', 'refresh');
   }

   function excluir($id){
	   if($this->model->excluir($id)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Post excluido com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir Post');
      }

      redirect('painel/blog', 'refresh');
   }

   function categorias(){
      $data['registros'] = $this->model->categorias();

      $data['titulo'] = 'Categorias do Blog';
      $data['unidade'] = "Categoria";
      $data['campo_1'] = "Título";

      if($this->session->flashdata('mostrarerro') === true)
         $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
      else
         $data['mostrarerro'] = false;
     
      if($this->session->flashdata('mostrarsucesso') === true)
         $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
      else
         $data['mostrarsucesso'] = false;      

      $this->load->view('painel/blog/categorias', $data);
   }

   function categoriasForm($id = false){
      if($id){
         $data['registro'] = $this->model->categorias($id);
         $data['titulo'] = 'Alterar Categoria do Blog';
         if(!$data['registro'])
            redirect('painel/blog/categorias');
      }else{
         $data['titulo'] = 'Inserir Categoria do Blog';
         $data['registro'] = FALSE;
      }
      
      $data['unidade'] = "Categoria";
      $this->load->view('painel/blog/categoriasForm', $data);
   }

   function categoriasInserir(){
      if($this->model->inserirCategoria()){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Categoria inserida com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir Categoria');
      }

      redirect('painel/blog/categorias', 'refresh');
   }

   function categoriasAlterar($id){
      if($this->model->alterarCategoria($id)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Categoria alterada com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar Categoria');
      }     
      redirect('painel/blog/categorias', 'refresh');
   }

   function categoriasExcluir($id){
      if($this->model->excluirCategoria($id)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Categoria excluida com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir Categoria');
      }

      redirect('painel/blog/categorias', 'refresh');
   }

   function imagens($id_post, $id_imagem = false){
      $post = $this->model->pegarPorId($id_post);

      $data['registros'] = $this->model->imagens($id_post);

      $data['titulo'] = 'Imagens do Post : '.$post->titulo;
      $data['unidade'] = "Imagens do Post : ".$post->titulo;
      $data['campo_1'] = "Imagem";

      $data['id_post'] = $id_post;

      if($id_imagem){
         $data['registro'] = $this->db->get_where('blog_imagens', array('id' => $id_imagem))->result();
      }

      $this->load->view('painel/blog/imagens', $data);      
   }

   function imagensInserir(){
      $id_post = $this->input->post('id_post');

      if($this->model->inserirImagem($id_post)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem inserida com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir Imagem');
      }

      redirect('painel/blog/imagens/'.$id_post, 'refresh');
   }

   function imagensAlterar($id){
      $id_post = $this->input->post('id_post');

      if($this->model->alterarImagem($id)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem alterada com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar Imagem');
      }

      redirect('painel/blog/imagens/'.$id_post, 'refresh');
   }   

   function imagensExcluir($id_imagem, $id_post){
      
      if($this->model->excluirImagem($id_imagem)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem excluida com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir Imagem');
      }

      redirect('painel/blog/imagens/'.$id_post, 'refresh');      
   }

   function comentarios($id_post){
      $data['atual'] = $this->model->pegarPorId($id_post);
      $data['comentarios'] = $this->model->comentarios($id_post);
      $data['titulo'] = "Comentários do Post : ".$data['atual']->titulo;
      $this->load->view('painel/blog/comentarios', $data);
   }

   function comentariosExcluir($id_comentario, $id_post){
      $this->db->delete('blog_comentarios', array('id' => $id_comentario));
      redirect('painel/blog/comentarios/'.$id_post);
   }
}