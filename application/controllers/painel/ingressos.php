<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ingressos extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Ingressos";
		$this->unidade = "Texto";
		$this->load->model('ingressos_model', 'model');
	}

}