<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Acao_social extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Ação Social";
		$this->unidade = "Texto";
		$this->load->model('acao_social_model', 'model');
	}

}