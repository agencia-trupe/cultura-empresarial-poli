<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Evento extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Evento";
		$this->unidade = "Texto";
		$this->load->model('evento_model', 'model');		
	}

}