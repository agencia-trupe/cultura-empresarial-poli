<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Evento extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
    	$this->load->model('evento_model', 'evento');
    	$data['evento'] = $this->evento->pegarTodos();
    	$data['evento'][0]->imagens = $this->evento->imagens($data['evento'][0]->id);
   		$this->load->view('evento', $data);
    }

}