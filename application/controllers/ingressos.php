<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ingressos extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
    	$this->load->model('ingressos_model', 'ingressos');
    	$data['registro'] = $this->ingressos->pegarTodos();
   		$this->load->view('ingressos', $data);
    }

}