<div id="marca">Realização: <img src="_imgs/layout/logo-poli-topo.png" alt="Poli Jr."></div>

<div id="banners">
	
	<?php if ($banners): ?>
		<?php foreach ($banners as $key => $value): ?>
			<?php if ($value->tipo=='link'): ?>
				<a href="<?=$value->destino?>" title="<?=$value->titulo?>">
					<img src="_imgs/banners/<?=$value->imagem?>">
				</a>
			<?php else: ?>
				<a href="<?=embed($value->destino,733,412,TRUE)?>" class="abre-video" title="<?=$value->titulo?>">
					<img src="_imgs/banners/<?=$value->imagem?>">
				</a>
			<?php endif ?>
		<?php endforeach ?>		
	<?php endif ?>	
	
</div>

<div id="banners-nav"></div>

<div class="conteudo">
	
	<h1>
		ACOMPANHE OS DESTAQUES &raquo;
	</h1>
	<div class="chamadas">
		<?php if ($chamadas): ?>
			<?php foreach ($chamadas as $key => $value): ?>
				<div class="container">
					<a href="<?=$value->destino?>" title="<?=$value->titulo.': '.$value->descricao?>">
						<img src="_imgs/chamadas/<?=$value->imagem?>" alt="<?=$value->titulo.': '.$value->descricao?>">
						<div class="texto"><span class="laranja"><?=$value->titulo?>: </span><?=$value->descricao?></div>
					</a>
				</div>
			<?php endforeach ?>
		<?php endif ?>
	</div>

	<h2>
		SIGA O EVENTO NO FACEBOOK &raquo;
	</h2>
	<div class="facebook">
		<div class="fb-like-box" data-href="https://www.facebook.com/pages/Semana-de-Cultura-Empresarial/142522385811099" data-width="812" data-height="294" data-show-faces="true" data-stream="false" data-show-border="true" data-header="true"></div>
	</div>

</div>