<div class="h600">
	<h1>Contato</h1>
	
	<?php if ($this->session->flashdata('contato_ok')): ?>
		
		<div class="enviado">
			<h2>Obrigado por entrar em contato!<br>Responderemos assim que possível.</h2>
		</div>

	<?else:?>

		<form action="contato/enviar" method="post" id="form-contato">
			<label>
				Nome
				<input type="text" name="nome" required>
			</label>
			<label>
				E-mail
				<input type="email" name="email" required>
			</label>
			<label>
				Telefone
				<input type="text" name="telefone">
			</label>
			<label>
				Mensagem
				<textarea name="mensagem" required></textarea>
			</label>
			<div class="submit-placeholder">
				<input type="submit" value="enviar">
			</div>
		</form>	

	<?endif;?>

	<div class="contato">
		<span class="telefone"><?=$contato[0]->telefone?></span>
		<span class="fax"><?=$contato[0]->fax?></span><br>
		<span class="email"><a href="mailto:<?=$contato[0]->email?>" title="Mande um email"><?=$contato[0]->email?></a></span>
		<br><br>
		<?=nl2br($como_chegar[0]->endereco)?>
	</div>
</div>