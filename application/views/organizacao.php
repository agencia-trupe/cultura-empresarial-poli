<div class="h600">
	<h1>Organização</h1>
	
	<div class="container">
		<?php if ($organizacao['realizacao']): ?>
			<div class="skew">
				<h2>REALIZAÇÃO</h2>
			</div>
			<?php foreach ($organizacao['realizacao'] as $key => $value): ?>
				<a href="organizacao/detalhes/<?=$value->id?>" class='fancy-organ center' title="<?=$value->titulo?>">
					<img src="_imgs/organizacao/<?=$value->imagem_fundo_branco?>" alt="<?=$value->titulo?>">
				</a>
			<?php endforeach ?>
		<?php endif ?>

		<?php if ($organizacao['patrocinio']): ?>
			<div class="skew xtramg">
				<h2>PATROCÍNIO</h2>
			</div>
			<?php foreach ($organizacao['patrocinio'] as $key => $value): ?>
				<a href="organizacao/detalhes/<?=$value->id?>" class='fancy-organ terco' title="<?=$value->titulo?>">
					<img src="_imgs/organizacao/<?=$value->imagem_fundo_branco?>" alt="<?=$value->titulo?>">
				</a>
			<?php endforeach ?>
		<?php endif ?>

		<?php if ($organizacao['apoio']): ?>
			<div class="skew xtramg">
				<h2>APOIO</h2>
			</div>
			<?php foreach ($organizacao['apoio'] as $key => $value): ?>
				<a href="organizacao/detalhes/<?=$value->id?>" class='fancy-organ terco' title="<?=$value->titulo?>">
					<img src="_imgs/organizacao/<?=$value->imagem_fundo_branco?>" alt="<?=$value->titulo?>">
				</a>
			<?php endforeach ?>
		<?php endif ?>
	</div>
</div>