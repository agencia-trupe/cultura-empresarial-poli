<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>  

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título<br>
		<input type="text" name="titulo" value="<?=$registro->titulo?>"></label>
		
		<label>Tipo<br>
		<select name="tipo" id="sel-tipo">
			<option value="link" <?if($registro->tipo=='link')echo" selected"?>>Link</option>
			<option value="video" <?if($registro->tipo=='video')echo" selected"?>>Vídeo</option>
		</select>
		</label>

		<label>Destino<br>
		<input type="text" name="destino" value="<?=$registro->destino?>"></label>

		<label>Imagem<br>
			<?if($registro->imagem):?>
				<img src="_imgs/banners/<?=$registro->imagem?>" style="max-width:400px"><br>
			<?endif;?>
		<input type="file" name="userfile"></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>	
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título<br>
		<input type="text" name="titulo"></label>
		
		<label>Tipo<br>
		<select name="tipo" id="sel-tipo">
			<option value="link">Link</option>
			<option value="video">Vídeo</option>
		</select>
		</label>

		<label>Destino<br>
		<input type="text" name="destino"></label>

		<label>Imagem<br>
		<input type="file" name="userfile"></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>