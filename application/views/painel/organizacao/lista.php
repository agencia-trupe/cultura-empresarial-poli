<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?> <a href="painel/<?=$this->router->class?>/form" class="btn btn-success">Adicionar <?=$unidade?></a>
    </h2>
  </div>  

  <div class="row">
    <div class="span12 columns">

      <div class="btn-group">
        <a href="painel/organizacao/index/todos" class="btn <?if($ordenacao=='todos')echo'active'?>">Todos</a>
        <a href="painel/organizacao/index/realizacao" class="btn <?if($ordenacao=='realizacao')echo'active'?>">Realização</a>
        <a href="painel/organizacao/index/patrocinio" class="btn <?if($ordenacao=='patrocinio')echo'active'?>">Patrocínio</a>
        <a href="painel/organizacao/index/apoio" class="btn <?if($ordenacao=='apoio')echo'active'?>">Apoio</a>
      </div>
      
      <br><br>

      <?php if ($registros): ?>

        <table class="table table-striped table-bordered table-condensed table-sortable" data-tabela="organizacao">

          <thead>
            <tr>
              <?if($ordenacao!='todos'):?><th>Ordenar</th><?endif;?>
              <th class="yellow header headerSortDown">Título</th>
              <th class="header">Marca (fundo branco / fundo transparente)</th>
              <th class="header">Tipo</th>
              <th class="red header">Ações</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($registros as $key => $value): ?>
              
                <tr class="tr-row" id="row_<?=$value->id?>">
                  <?if($ordenacao!='todos'):?><td class="move-actions"><a href="#" class="btn btn-info btn-move">mover</a></td><?endif;?>
                  <td><?=$value->titulo?></td>
                  <td><img src="_imgs/organizacao/<?=$value->imagem_fundo_branco?>"><img src="_imgs/organizacao/<?=$value->imagem_fundo_transparente?>"></td>
                  <td><?=tipo($value->categoria)?></td>
                  <td class="crud-actions">
                    <a href="painel/<?=$this->router->class?>/form/<?=$value->id?>" class="btn btn-primary">editar</a>
                    <a href="painel/<?=$this->router->class?>/excluir/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                  </td>
                </tr>

            <?php endforeach ?>
          </tbody>

        </table>

      <?php else:?>

      	<h3>Nenhum Registro</h2>

      <?php endif ?>

    </div>
  </div>