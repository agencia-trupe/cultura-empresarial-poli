<div class="navbar navbar-fixed-top">

  <div class="navbar-inner">

    <div class="container">

      <a href="painel/home" class="brand">SCE Poli Jr</a>

      <ul class="nav">

        <li <?if($this->router->class=='home')echo" class='active'"?>><a href="painel/home">Início</a></li>

        <li class="dropdown  <?if($this->router->class=='banners' || $this->router->class=='chamadas')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Home <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/banners/">Banners</a></li>
            <li><a href="painel/chamadas/">Chamadas</a></li>
          </ul>
        </li>

        <li <?if($this->router->class=='evento')echo" class='active'"?>><a href="painel/evento">Evento</a></li>

        <li class="dropdown <?if($this->router->class=='')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Programação <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/cronograma">Cronograma</a></li>
            <li><a href="painel/oficinas">Oficinas</a></li>
            <li><a href="painel/palestrantes">Palestrantes</a></li>
            <li><a href="painel/minicursos">Mini Cursos</a></li>
          </ul>
        </li>

        <li <?if($this->router->class=='ingressos')echo" class='active'"?>><a href="painel/ingressos">Ingressos</a></li>

        <li <?if($this->router->class=='organizacao')echo" class='active'"?>><a href="painel/organizacao">Organização</a></li>

        <li <?if($this->router->class=='acao_social')echo" class='active'"?>><a href="painel/acao_social">Ação Social</a></li>

        <li <?if($this->router->class=='como_chegar')echo" class='active'"?>><a href="painel/como_chegar">Como Chegar</a></li>

        <li <?if($this->router->class=='contato')echo" class='active'"?>><a href="painel/contato">Contato</a></li>

        <li class="dropdown <?if($this->router->class=='usuarios' || $this->router->class=='cadastros')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/cadastros">Cadastros de Newsletter</a></li>
            <li><a href="painel/usuarios">Usuários</a></li>
            <li><a href="painel/home/logout">Logout</a></li>
          </ul>
        </li>

      </ul>

    </div>
  </div>
</div>