<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>  

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título<br>
		<input type="text" name="titulo" required autofocus value="<?=$registro->titulo?>"></label>

		<label>Número de Vagas<br>
		<input type="number" name="n_vagas" required value="<?=$registro->n_vagas?>"></label>

		<label>Texto<br>
		<textarea name="texto" class="medio basico"><?=$registro->texto?></textarea></label>

		<label>Horário<br>
		<input type="text" name="horario" required value="<?=$registro->horario?>"></label>

		<label>Local<br>
		<input type="text" name="horario_detalhes" required value="<?=$registro->horario_detalhes?>"></label>

		<label>Data<br>
		<input type="text" name="data" class="datepicker" required value="<?=formataData($registro->data, 'mysql2br')?>"></label>

		<label>Por<br>
		<input type="text" name="por" required value="<?=$registro->por?>"></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>	
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título<br>
		<input type="text" name="titulo" required autofocus></label>

		<label>Número de Vagas<br>
		<input type="number" name="n_vagas" required></label>

		<label>Texto<br>
		<textarea name="texto" class="medio basico"></textarea></label>

		<label>Horário<br>
		<input type="text" name="horario" required></label>

		<label>Local<br>
		<input type="text" name="horario_detalhes" required></label>

		<label>Data<br>
		<input type="text" name="data" class="datepicker" required></label>

		<label>Por<br>
		<input type="text" name="por" required></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>