<div class="container top">

  <?if($this->session->flashdata('mostrarsucesso')):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$this->session->flashdata('mostrarsucesso_mensagem')?></div>
  <?elseif($this->session->flashdata('mostrarerro')):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$this->session->flashdata('mostrarerro_mensagem')?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?> <a href="painel/<?=$this->router->class?>/download/<?=$parent->id?>" class="btn btn-success">Extrair Inscrições</a>
    </h2>
  </div>

  <a href="painel/minicursos" title="Voltar"class="btn btn-default">&larr; Voltar</a>
  
  <h3 style="margin-top:30px;">Total de Vagas : <?=$parent->n_vagas?></h3>
  <h3>Inscritos : <?=$parent->inscritos?></h3>

  <hr style="clear:left;margin-top:15px;"> 

  <input type="text" placeholder="Buscar Inscrição (por Nome ou E-mail)" nome="buscaInscricao" id="searchInscricao">
  <ul id="resultados"></ul>

  <hr style="clear:left;margin-top:15px;"> 

  <div class="row" style="margin-top:30px;">
    <div class="span12 columns">

      <?php if ($inscricoes): ?>

        <table class="table table-striped table-bordered table-condensed">

          <thead>
            <tr>
              <th class="yellow header headerSortDown">Nome</th>
              <th class="header">Celular</th>
              <th class="header">Faculdade</th>
              <th class="header">Curso</th>
              <th class="header">E-mail</th>
              <th class="header">Data Inscrição</th>
              <th class="red header">Ações</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($inscricoes as $key => $value): ?>
              
                <tr class="tr-row" id="row_<?=$value->id?>">
                  <td><?=$value->nome?></td>
                  <td><?=$value->celular?></td>
                  <td><?=$value->faculdade?></td>
                  <td><?=$value->curso?></td>
                  <td><?=$value->email?></td>
                  <td><?=formataTimestamp($value->data_inscricao, true)?></td>
                  <td class="crud-actions">
                    <a href="painel/<?=$this->router->class?>/verInscricao/<?=$value->id?>/<?=$value->mini_cursos_id?>" id="viewInsc-<?=$value->id?>" class="btn btn-info btn-viewinsc">ver</a>
                    <a href="painel/<?=$this->router->class?>/excluirInscricao/<?=$value->id?>/<?=$value->mini_cursos_id?>" class="btn btn-danger btn-delete">excluir</a>
                  </td>                  
                </tr>

            <?php endforeach ?>
          </tbody>

        </table>

        <?php if (isset($paginacao) && $paginacao): ?>          
          <div class="pagination">
            <ul>        
              <?=$paginacao?>
            </ul>
          </div>
        <?php endif ?>

      <?php else:?>

      	<h3>Nenhum Registro</h2>

      <?php endif ?>

    </div>
  </div>