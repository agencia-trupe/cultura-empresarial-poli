<div style="width:600px; padding:30px;">
	<h1 style="padding-bottom:10px; margin-bottom:10px; border-bottom:1px #ccc solid;">Resumo de Inscrição</h1>

	<h2>Curso : <span style="color:#5bb75b;"><?=$curso->titulo?></span></h2>

	<h3>Nome: <span style="color:#5bb75b;"><?=$registro->nome?></span></h3>
	<h3>Celular: <span style="color:#5bb75b;"><?=$registro->celular?></span></h3>
	<h3>Faculdade: <span style="color:#5bb75b;"><?=$registro->faculdade?></span></h3>
	<h3>Curso: <span style="color:#5bb75b;"><?=$registro->curso?></span></h3>
	<h3>Ano de Ingresso: <span style="color:#5bb75b;"><?=$registro->ano_ingresso?></span></h3>
	<h3>Email: <span style="color:#5bb75b;"><a href="mailto:<?=$registro->email?>"><?=$registro->email?></a></span></h3>
	<h3>Data de Inscrição: <span style="color:#5bb75b;"><?=formataTimestamp($registro->data_inscricao, true)?></span></h3>
	
</div>