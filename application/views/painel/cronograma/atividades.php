<div class="container top">

  <ul class="breadcrumb">
    <li>
      <a href="painel/cronograma">Cronograma</a> <span class="divider">/</span>
    </li>
    <li class="active">
      <a href="painel/cronograma/atividades/<?=$parent->id?>"><?=$titulo?></a>
    </li>    
  </ul>

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?> <a href="painel/<?=$this->router->class?>/formAtividades/<?=$parent->id?>" class="btn btn-success">Adicionar <?=$unidade?></a>
    </h2>
  </div>  

  <div class="row">
    <div class="span12 columns">

    	<a href="painel/cronograma" class="btn btn-voltar">Voltar</a><br><br>

      <?php if ($atividades): ?>

        <table class="table table-striped table-bordered table-condensed table-sortable">

          <thead>
            <tr>
              <th class="yellow header headerSortDown">Horário</th>
              <th class="header">Palestrante</th>
              <th class="header">Título</th>
              <th class="header">Tipo</th>
              <th class="red header">Ações</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($atividades as $key => $value): ?>
              
                <tr class="tr-row" id="row_<?=$value->id?>">
                  <td><?=horario($value->horario)?></td>
                  <td>
                    <?php
                      if($value->palestrante_tipo == 'texto_livre' || $value->palestrante_tipo == 'a_definir'){
                        echo $value->palestrante_detalhe;
                      }elseif($value->palestrante_tipo == 'cadastrado'){
                        echo palestrante($value->palestrante_detalhe);
                      }elseif($value->palestrante_tipo == 'sem_palestrante'){
                        echo "--";
                      }
                    ?>
                  </td>
                  <td>
                    <?php
                      if($value->titulo_tipo == 'oficina'){
                        echo tituloAtividade($value->titulo_detalhe, false, 'oficina');
                      }elseif($value->titulo_tipo == 'minicurso'){
                        echo tituloAtividade($value->titulo_detalhe, false, 'minicurso');
                      }else{
                        echo $value->titulo_detalhe;
                      }                      
                    ?>
                  </td>
                  <td><?=$value->tipo?></td>
                  <td class="crud-actions">
                    <a href="painel/<?=$this->router->class?>/formAtividades/<?=$parent->id?>/<?=$value->id?>" class="btn btn-primary">editar</a>
                    <a href="painel/<?=$this->router->class?>/excluirAtividade/<?=$value->id?>/<?=$parent->id?>" class="btn btn-danger btn-delete">excluir</a>
                  </td>
                </tr>

            <?php endforeach ?>
          </tbody>

        </table>


      <?php else:?>

      	<h3>Nenhum Registro</h2>

      <?php endif ?>



    </div>
  </div>