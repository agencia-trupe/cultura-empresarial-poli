<div class="container top">

  <ul class="breadcrumb">
    <li>
      <a href="painel/cronograma">Cronograma</a> <span class="divider">/</span>
    </li>
    <li>
      <a href="painel/cronograma/atividades/<?=$parent->id?>"><?=$titulo_parent?></a> <span class="divider">/</span>
    </li>
	<li class="active">
      <a href="<?=current_url()?>"><?=$titulo?></a>
    </li>    
  </ul>

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>  

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/editarAtividade/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Horário<br>
		<input type="text" name="horario" class="timepicker" required value="<?=horario($registro->horario)?>"></label>

		<br>

		Palestrante<br>
		<label>
			<input type="radio" name="palestrante_tipo" value="cadastrado" <?if($registro->palestrante_tipo == 'cadastrado')echo" checked"?>>Selecionar Palestrante Cadastrado
			<select name="palestrante_detalhe_id" class="show hid pal sel">
				<?php if ($palestrantes): ?>
					<?php foreach ($palestrantes as $key => $value): ?>
						<option value="<?=$value->id?>"<?if($registro->palestrante_detalhe == $value->id)echo" selected"?>><?=$value->nome?></option>		
					<?php endforeach ?>	
				<?php else:?>
					<option value="0">Nenhum palestrante cadastrado</option>
				<?php endif ?>
			</select>
		</label>

		<label>
			<input type="radio" name="palestrante_tipo" value="texto_livre" <?if($registro->palestrante_tipo=='texto_livre')echo" checked"?>>Texto Livre
			<input type="text" name="palestrante_detalhe_txt" class="show hid pal txt" <?if($registro->palestrante_tipo=='texto_livre')echo" value='".$registro->palestrante_detalhe."'"?>>
		</label>

		<label>
			<input type="radio" name="palestrante_tipo" value="a_definir"<?if($registro->palestrante_tipo == 'a_definir')echo" checked"?>>A definir
		</label>

		<label>
			<input type="radio" name="palestrante_tipo" value="sem_palestrante"<?if($registro->palestrante_tipo == 'sem_palestrante')echo" checked"?>>A Atividade não possui palestrante
		</label>

		<br><br>

		Título<br>
		<label>
			<input type="radio" name="titulo_tipo" value="oficina" <?if($registro->titulo_tipo=='oficina')echo" checked"?>>Selecionar Oficina Cadastrada
			<select name="titulo_detalhe_oficina_id" class="show hid tit">
				<?php if ($oficinas): ?>
					<?php foreach ($oficinas as $key => $value): ?>
						<option value="<?=$value->id?>" <?if($value->id==$registro->titulo_detalhe)echo" selected"?>><?=$value->titulo?></option>		
					<?php endforeach ?>	
				<?php else: ?>
					<option value="0">Nenhuma oficina cadastrada</option>
				<?php endif ?>
			</select>
		</label>

		<label>
			<input type="radio" name="titulo_tipo" value="minicurso" <?if($registro->titulo_tipo=='minicurso')echo" checked"?>>Selecionar Oficina Cadastrada
			<select name="titulo_detalhe_minicurso_id" class="show hid tit">
				<?php if ($minicursos): ?>
					<?php foreach ($minicursos as $key => $value): ?>
						<option value="<?=$value->id?>" <?if($value->id==$registro->titulo_detalhe)echo" selected"?>><?=$value->titulo?></option>		
					<?php endforeach ?>	
				<?php else: ?>
					<option value="0">Nenhum Mini Curso cadastrado</option>
				<?php endif ?>
			</select>
		</label>
		
		<label>
			<input type="radio" name="titulo_tipo" value="texto_livre" <?if($registro->titulo_tipo=='texto_livre')echo" checked"?>>Texto Livre / Nome de Palestra
			<input type='text' name="titulo_detalhe_txt" class="show hid tit" <?if($registro->titulo_tipo=='texto_livre')echo" value='".$registro->titulo_detalhe."'"?>>
		</label>		

		<br><br>

		<input type="hidden" name="data_parent" value="<?=$parent->data?>">

		<input type="hidden" name="parent_id" value="<?=$parent->id?>">

		<label>Tipo<br>
			<select name="tipo">
				<option value="" <?if($registro->tipo=='')echo" selected"?>></option>
				<option value="oficina" <?if($registro->tipo=='oficina')echo" selected"?>>oficina</option>
				<option value="palestra" <?if($registro->tipo=='palestra')echo" selected"?>>palestra</option>
				<option value="minicurso" <?if($registro->tipo=='minicurso')echo" selected"?>>mini-curso</option>
			</select>
		</label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>	
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserirAtividade')?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Horário<br>
		<input type="text" name="horario" class="timepicker" required></label>
		
		<br>

		Palestrante<br>
		<label>
			<input type="radio" name="palestrante_tipo" value="cadastrado">Selecionar Palestrante Cadastrado
			<select name="palestrante_detalhe_id" class="show hid pal">
				<?php if ($palestrantes): ?>
					<?php foreach ($palestrantes as $key => $value): ?>
						<option value="<?=$value->id?>"><?=$value->nome?></option>		
					<?php endforeach ?>	
				<?php else:?>
					<option value="0">Nenhum palestrante cadastrado</option>
				<?php endif ?>
			</select>
		</label>

		<label>
			<input type="radio" name="palestrante_tipo" value="texto_livre">Texto Livre
			<input type="text" name="palestrante_detalhe_txt" class="show hid pal">
		</label>

		<label>
			<input type="radio" name="palestrante_tipo" value="a_definir">A definir
		</label>

		<label>
			<input type="radio" name="palestrante_tipo" value="sem_palestrante">A Atividade não possui palestrante
		</label>

		<br><br>

		Título<br>
		<label>
			<input type="radio" name="titulo_tipo" value="oficina">Selecionar Oficina Cadastrada
			<select name="titulo_detalhe_oficina_id" class="show hid tit">
				<?php if ($oficinas): ?>
					<?php foreach ($oficinas as $key => $value): ?>
						<option value="<?=$value->id?>"><?=$value->titulo?></option>		
					<?php endforeach ?>	
				<?php else: ?>
					<option value="0">Nenhuma oficina cadastrada</option>
				<?php endif ?>
			</select>
		</label>

		<label>
			<input type="radio" name="titulo_tipo" value="minicurso">Selecionar Mini Curso Cadastrado
			<select name="titulo_detalhe_minicurso_id" class="show hid tit">
				<?php if ($minicursos): ?>
					<?php foreach ($minicursos as $key => $value): ?>
						<option value="<?=$value->id?>"><?=$value->titulo?></option>		
					<?php endforeach ?>	
				<?php else: ?>
					<option value="0">Nenhum Mini Curso cadastrado</option>
				<?php endif ?>
			</select>
		</label>
		
		<label>
			<input type="radio" name="titulo_tipo" value="texto_livre">Texto Livre / Nome de Palestra
			<input type='text' name="titulo_detalhe_txt" class="show hid tit">
		</label>		

		<br><br>

		<label>Tipo<br>
			<select name="tipo">
				<option value=""></option>
				<option value="oficina">oficina</option>
				<option value="palestra">palestra</option>
				<option value="minicurso">minicurso</option>
			</select>
		</label>

		<input type="hidden" name="data_parent" value="<?=$parent->data?>">

		<input type="hidden" name="parent_id" value="<?=$parent->id?>">

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>