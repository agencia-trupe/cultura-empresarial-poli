<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>  

  <div class="row">
    <div class="span12 columns">

      <?php if ($registros): ?>

        <table class="table table-striped table-bordered table-condensed table-sortable">

          <thead>
            <tr>
              <th class="yellow header headerSortDown">Email</th>
              <th class="header">Telefone</th>
              <th class="red header">Fax</th>
              <th class="red header">Flickr</th>
              <th class="red header">Facebook</th>
              <th class="red header">YouTube</th>
              <th class="red header">Ações</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($registros as $key => $value): ?>
              
                <tr class="tr-row" id="row_<?=$value->id?>">
                  <td><?=$value->email?></td>
                  <td><?=$value->telefone?></td>
                  <td><?=$value->fax?></td>
                  <td>
                    <?if($value->flickr):?>
                      <img src="css/painel/accept.png">
                    <?else:?>
                      <img src="css/painel/cross.png">
                    <?endif;?>
                  </td>
                  <td>
                    <?if($value->facebook):?>
                      <img src="css/painel/accept.png">
                    <?else:?>
                      <img src="css/painel/cross.png">
                    <?endif;?>
                  </td>
                  <td>
                    <?if($value->youtube):?>
                      <img src="css/painel/accept.png">
                    <?else:?>
                      <img src="css/painel/cross.png">
                    <?endif;?>
                  </td>
                  <td class="crud-actions" style="width:60px;">
                    <a href="painel/<?=$this->router->class?>/form/<?=$value->id?>" class="btn btn-primary">editar</a>                    
                  </td>
                </tr>

            <?php endforeach ?>
          </tbody>

        </table>

        <?php if (isset($paginacao) && $paginacao): ?>          
          <div class="pagination">
            <ul>        
              <?=$paginacao?>
            </ul>
          </div>
        <?php endif ?>

      <?php else:?>

      	<h3>Nenhum Registro</h2>

      <?php endif ?>



    </div>
  </div>