<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>  

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Email de contato<br>
		<input type="text" name="email" value="<?=$registro->email?>"></label>

		<label>Telefone<br>
		<input type="text" name="telefone" value="<?=$registro->telefone?>"></label>

		<label>Fax<br>
		<input type="text" name="fax" value="<?=$registro->fax?>"></label>

		<label>Flickr<br>
		<input type="text" name="flickr" value="<?=$registro->flickr?>"></label>

		<label>Facebook<br>
		<input type="text" name="facebook" value="<?=$registro->facebook?>"></label>

		<label>YouTube<br>
		<input type="text" name="youtube" value="<?=$registro->youtube?>"></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>	

<?endif ?>