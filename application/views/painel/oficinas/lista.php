<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?> <a  href="painel/<?=$this->router->class?>/form" class="btn btn-success">Adicionar <?=$unidade?></a>
    </h2>
  </div>  

  <input type="text" placeholder="Buscar Inscrição (por Nome ou E-mail)" nome="buscaInscricao" id="searchInscricao" data-tipo='oficinas'>
  <ul id="resultados"></ul>

  <hr style="clear:left;margin-top:15px;">   

  <div class="row">
    <div class="span12 columns">

      <?php if ($registros): ?>

        <table class="table table-striped table-bordered table-condensed table-sortable" data-tabela="oficinas">

          <thead>
            <tr>
              <th>Ordenar</th>
              <th class="yellow header headerSortDown">Título</th>
              <th class="header">Texto</th>
              <th class="header">Inscrições</th>
              <th class="header">Data</th>
              <th class="header">Por</th>
              <th class="red header">Ações</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($registros as $key => $value): ?>
              
                <tr class="tr-row" id="row_<?=$value->id?>">
                  <td class="move-actions"><a href="#" class="btn btn-info btn-move">mover</a></td>
                  <td><?=$value->titulo?></td>
                  <td><?=word_limiter(strip_tags($value->texto), 15)?></td>
                  <td style="text-align:center;">
                    <?=$value->n_inscricoes.'/'.$value->n_vagas?><br>
                    <a href="painel/<?=$this->router->class?>/inscricoes/<?=$value->id?>" class="btn btn-info">inscrições</a>
                  </td>
                  <td><?=formataData($value->data, 'mysql2br')?></td>
                  <td><?=$value->por?></td>
                  <td class="crud-actions">
                    <a href="painel/<?=$this->router->class?>/form/<?=$value->id?>" class="btn btn-primary">editar</a>
                    <a href="painel/<?=$this->router->class?>/excluir/<?=$value->id?>" class="btn btn-danger btn-delete">excluir</a>
                  </td>
                </tr>

            <?php endforeach ?>
          </tbody>

        </table>

        <?php if (isset($paginacao) && $paginacao): ?>          
          <div class="pagination">
            <ul>        
              <?=$paginacao?>
            </ul>
          </div>
        <?php endif ?>

      <?php else:?>

      	<h3>Nenhum Registro</h2>

      <?php endif ?>

    </div>
  </div>