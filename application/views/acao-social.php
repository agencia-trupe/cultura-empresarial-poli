<div class="h600">
	<h1>Ação Social</h1>

	<div class="olho"><?=$acaosocial[0]->olho?></div>

	<div class="texto"><?=str_replace('../../../', '', $acaosocial[0]->texto)?></div>
</div>

<div class="galeria">

	<?php if ($acaosocial[0]->imagens): ?>

		<a class="prev" href="#" title="Imagem Anterior"></a>

		<div class="animate">
			<?php foreach ($acaosocial[0]->imagens as $key => $value): ?>
				<img src="_imgs/acao_social/<?=$value->imagem?>">
			<?php endforeach ?>
		</div>

		<a class="next" href="#" title="Próxima Imagem"></a>

	<?php endif ?>

</div>

<div id="pager"></div>