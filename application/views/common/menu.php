<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">

<div id="fundo-azul"></div>
<header>

	<a href="<?=base_url()?>" id="logo-link" title="Página Inicial"><img src="_imgs/layout/logo-23SCE-poljr.png" alt="Semana de Cultura Empresarial Poli Jr."></a>

	<nav>
		<ul>
			<li id="mn-home"><a href="home" title="Página Inicial" <?if($this->router->class=='home')echo" class='ativo'"?>>HOME</a></li>
			<li id="mn-evento"><a href="evento" title="O evento" <?if($this->router->class=='evento')echo" class='ativo'"?>>O EVENTO</a></li>
			<li id="mn-programacao"><a href="programacao" title="Programação" <?if($this->router->class=='programacao')echo" class='ativo'"?>>PROGRAMAÇÃO</a></li>
			<li id="mn-ingressos"><a href="ingressos" title="Ingressos" <?if($this->router->class=='ingressos')echo" class='ativo'"?>>INGRESSOS</a></li>
			<li id="mn-organizacao"><a href="organizacao" title="Organização" <?if($this->router->class=='organizacao')echo" class='ativo'"?>>ORGANIZAÇÃO</a></li>
			<li id="mn-acaosocial"><a href="acao-social" title="Ação Social" <?if($this->router->class=='acaosocial')echo" class='ativo'"?>>AÇÃO SOCIAL</a></li>
			<li id="mn-comochegar"><a href="como-chegar" title="Como Chegar" <?if($this->router->class=='comochegar')echo" class='ativo'"?>>COMO CHEGAR</a></li>
			<li id="mn-contato"><a href="contato" title="Contato" <?if($this->router->class=='contato')echo" class='ativo'"?>>CONTATO</a></li>
		</ul>
	</nav>
	<?if($this->router->class=='programacao'):?>
		<ul class="submenu">
			<li><a href="programacao/cronograma" <?if($this->router->class=='programacao' && ($this->router->method=='cronograma' || $this->router->method=='index'))echo" class='ativo'"?> title="Cronograma">CRONOGRAMA</a></li>
			<li><a href="programacao/oficinas" <?if($this->router->class=='programacao' && $this->router->method=='oficinas')echo" class='ativo'"?> title="Oficinas">OFICINAS</a></li>
			<li><a href="programacao/palestrantes" <?if($this->router->class=='programacao' && $this->router->method=='palestrantes')echo" class='ativo'"?> title="Palestrantes">PALESTRANTES</a></li>
			<li><a href="programacao/minicursos" <?if($this->router->class=='programacao' && ($this->router->method=='minicursos' || $this->router->method=='inscricaoMinicursos'))echo" class='ativo'"?> title="Mini Cursos">MINI CURSOS</a></li>			
		</ul>
	<?endif;?>

	<?php if ($contato[0]->flickr): ?>
		<a href="<?=prep_url($contato[0]->flickr)?>" title="Nosso Flickr" class="social-links" target="_blank"><img src="_imgs/layout/icone-flickr.png" alt="Flickr"></a>
	<?php endif ?>

	<?php if ($contato[0]->facebook): ?>
		<a href="<?=prep_url($contato[0]->facebook)?>" title="Nossa página no Facebook" class="social-links" target="_blank"><img src="_imgs/layout/icone-facebook.png" alt="Facebook"></a>
	<?php endif ?>

	<?php if ($contato[0]->youtube): ?>
		<a href="<?=prep_url($contato[0]->youtube)?>" title="Nosso canal no YouTube" class="social-links" target="_blank"><img src="_imgs/layout/icone-youtube.png" alt="YouTube"></a>
	<?php endif ?>
	
</header>