<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Semana de Cultura Empresarial Poli Jr.</title>
  <meta name="description" content="Semana de Cultura Empresarial <?=date('y')?> - Na Escola Politécnica da USP">
  <meta name="keywords" content="semana, cultural, empresarial, <?=date('y')?>, poli, jr, são paulo, usp, politécnica" />
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2013 Trupe Design" />
  <meta name="language" content="pt-BR" />

  <meta name="viewport" content="width=device-width,initial-scale=1">

  <meta property="og:title" content="SCE - Semana de Cultura Empresarial <?=date('y')?>"/>
  <meta property="og:site_name" content="SCE - Semana de Cultura Empresarial"/>
  <meta property="og:type" content="website"/>
  <meta property="og:image" content="<?=base_url('_imgs/layout/fb.jpg')?>"/>
  <meta property="og:url" content="<?=base_url()?>"/>
  <meta property="og:description" content="Semana de Cultura Empresarial <?=date('y')?> - Na Escola Politécnica da USP"/>

  <base href="<?= base_url() ?>">
  <script> var BASE = '<?= base_url() ?>'</script>

  <?CSS(array('reset', 'base', 'fontface/stylesheet', 'fancybox/fancybox', $this->router->class, $load_css))?>  
  

  <?if(ENVIRONMENT == 'development'):?>
    
    <?JS(array('modernizr-2.0.6.min', 'less-1.3.0.min', 'jquery-1.8.0.min', $this->router->class, $load_js))?>
    
  <?else:?>

    <?JS(array('modernizr-2.0.6.min', $this->router->class, $load_js))?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.8.0.min.js"><\/script>')</script>

  <?endif;?>

</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=419942241422773";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>