
  </div> <!-- fim da div main -->

  <footer>

    <div class="superior">

        <div class="centro">
            <div class="realizacao">
                <h3>Realização</h3>
                <?php if ($organizacao['realizacao']): ?>
                    <span>
                    <?php foreach ($organizacao['realizacao'] as $key => $value): ?>                        
                        <img src="_imgs/organizacao/<?=$value->imagem_fundo_transparente?>" alt="<?=$value->titulo?>">                        
                    <?php endforeach ?>
                    </span>
                <?php endif ?>
            </div>

            <div class="patrocinio">
                <h3>Patrocínio</h3>
                <?php if ($organizacao['patrocinio']): ?>
                    <span>
                    <?php foreach ($organizacao['patrocinio'] as $key => $value): ?>                        
                        <img src="_imgs/organizacao/<?=$value->imagem_fundo_transparente?>" alt="<?=$value->titulo?>">                        
                    <?php endforeach ?>
                    </span>
                <?php endif ?>
            </div>

            <div class="apoio">
                <h3>Apoio</h3>
                <?php if ($organizacao['apoio']): ?>
                    <span>
                    <?php foreach ($organizacao['apoio'] as $key => $value): ?>                        
                        <img src="_imgs/organizacao/<?=$value->imagem_fundo_transparente?>" alt="<?=$value->titulo?>">                        
                    <?php endforeach ?>
                    </span>
                <?php endif ?>
            </div>
      </div>
    </div>

    <div class="inferior">
        <div class="centro">
            <div class="contato">
                <?php if ($contato[0]->telefone): ?>
                    <div class="telefone"><?=$contato[0]->telefone?></div>                    
                <?php endif ?>
                <?php if ($contato[0]->email): ?>
                    <div class="email"><a href="mailto:<?=$contato[0]->email?>" title="Entre em contato"><?=$contato[0]->email?></a></div>                    
                <?php endif ?>
            </div>
            <div class="endereco">
                <?php if ($como_chegar[0]->endereco): ?>
                    <?=nl2br($como_chegar[0]->endereco)?>
                <?php endif ?>
            </div>
            <div class="assinatura">
                <a href="http://www.trupe.net" target="_blank" title="CRIAÇÃO DE SITES: TRUPE AGÊNCIA CRIATIVA"><span>CRIAÇÃO DE SITES: TRUPE AGÊNCIA CRIATIVA</span><img src="_imgs/layout/kombi.png" alt="TRUPE AGÊNCIA CRIATIVA"></a>
            </div>
        </div>
    </div>
  
  </footer>
  
  
  <script>
    window._gaq = [['_setAccount','UA-1031947-35'],['_trackPageview'],['_trackPageLoadTime']];
    Modernizr.load({
       load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
    });
  </script>
  
  <?JS(array('cycle','maskedinput','fancybox','front'))?>
  
</body>
</html>
