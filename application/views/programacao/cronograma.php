<div class="h600">
	<h1>Cronograma</h1>

	<?php if ($dias): ?>
		
		<div class="divs-container">

			<?php foreach ($dias as $key => $value): ?>

				<div class="dia">
					
					<div class="skew">
						<h2><?=mb_strtoupper(diaDaSemana(date('w', strtotime($value->data))))?> <span class="data"><?=date('d/m', strtotime($value->data))?></span></h2>
					</div>

					<?php if ($value->atividades): ?>

						<table>

							<thead>
								<tr>
									<th class="hor">Horário</th>
									<th class="pal">Palestrante</th>
									<th class="tit">Título</th>
									<th>Tipo</th>
								</tr>
							</thead>
							<tbody>						
							<?php foreach ($value->atividades as $key => $value): ?>
							
								<tr>
									<td><?=horario($value->horario)?></td>
									<td>
										<?php
					                    if($value->palestrante_tipo == 'texto_livre' || $value->palestrante_tipo == 'a_definir'){
					                    	echo $value->palestrante_detalhe;
					                    }elseif($value->palestrante_tipo == 'cadastrado'){
					                    	echo palestrante($value->palestrante_detalhe);
					                    }elseif($value->palestrante_tipo == 'sem_palestrante'){
					                    	echo "A Atividade não possui palestrante";
					                    }
					                    ?>
									</td>
									<td>
										<?php
											if($value->titulo_tipo == 'oficina'){
												echo tituloAtividade($value->titulo_detalhe, true, 'oficina');
											}elseif($value->titulo_tipo == 'minicurso'){
												echo tituloAtividade($value->titulo_detalhe, true, 'minicurso');
											}else{
												echo $value->titulo_detalhe;
											}                      
										?>
									</td>
									<td><?=$value->tipo?></td>
								</tr>						

							<?php endforeach ?>
							</tbody>
						</table>

					<?php endif ?>

				</div>

			<?php endforeach ?>

		</div>

	<?php endif ?>
</div>