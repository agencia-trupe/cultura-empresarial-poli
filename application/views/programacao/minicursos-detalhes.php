<div class="oficina-det">
	
	<div class="skew courtain">
		<h2><?=mb_strtoupper($detalhes->titulo)?> <div class="data"><?=date('d/m', strtotime($detalhes->data))?></div> <div class="seta"></div> </h2>
	</div>
	
	<div class="detalhes-oficina hidden">
		
		<h3>Por <?=$detalhes->por?></h3>

		<div class="texto">
			<?=$detalhes->texto?>
		</div>

		<div class="vagas">
			<div class="numero">VAGAS: <span><?=str_pad($detalhes->vagas, 4, '0', STR_PAD_LEFT)?></span></div>
			<a href="programacao/inscricao-minicursos/<?=$detalhes->id?>" title="RESERVAR INGRESSO">RESERVAR INGRESSO <img src="_imgs/layout/icone-ingresso.png" alt="Reserve seu ingresso"></a>
		</div>

		<?php if ($detalhes->horario): ?>
			<div class="skew-cinza">
				<div class="horario">
					<span class='label'>Horários:</span><br>
					<?=$detalhes->horario?>
					<?php if ($detalhes->horario_detalhes): ?>
						<small><?=$detalhes->horario_detalhes?></small>
					<?php endif ?>
				</div>
			</div>
		<?php endif ?>

	</div>
	

</div>