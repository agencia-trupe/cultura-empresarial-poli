<div class="h600">
	<h1>Reserve seu ingresso</h1>
	<h2>Cadastre-se abaixo para garantir seu ingresso no evento.</h2>
	<form action="" method="post" id="formInscricao">
		
		<div id="cursoSel">
			
			<select name="oficinas_id" required id="selMiniCursosId">
				<option value="" data-vagas="0">SELECIONE O EVENTO</option>
				<?php if($oficinas): ?>
					<?php foreach($oficinas as $mini): ?>
						<option value="<?=$mini->id?>" data-vagas="<?=$mini->vagas?>" <?php if($mini->id==$cursoSel) echo "selected" ?>><?=$mini->titulo?></option>
					<?php endforeach; ?>
				<?php endif; ?>
			</select>

			<div class="vagasSel">
				<div class="numero">VAGAS: <span><?php echo $vagasSel ?></span></div>				
			</div>

		</div>

		<div id="dadosInscricao">

			<div class="form-row">
				<label for="inputNome">Nome</label>
				<input type="text" name="nome" id="inputNome" required value="<?php if($this->session->flashdata('nome')) echo $this->session->flashdata('nome') ?>">
			</div>

			<div class="form-row">
				<label for="inputCelular">Celular</label>
				<input type="text" name="celular" id="inputCelular" required value="<?php if($this->session->flashdata('celular')) echo $this->session->flashdata('celular') ?>">
			</div>

			<div class="form-row">
				<label for="inputFaculdade">Faculdade</label>
				<input type="text" name="faculdade" id="inputFaculdade" required value="<?php if($this->session->flashdata('faculdade')) echo $this->session->flashdata('faculdade') ?>">
			</div>

			<div class="form-row">
				<label for="inputCurso">Curso</label>
				<input type="text" name="curso" id="inputCurso" required value="<?php if($this->session->flashdata('curso')) echo $this->session->flashdata('curso') ?>">
			</div>

			<div class="form-row">
				<label for="inputAnoIngresso">Ano de Ingresso</label>
				<input type="text" name="anoIngresso" id="inputAnoIngresso" required value="<?php if($this->session->flashdata('anoIngresso')) echo $this->session->flashdata('anoIngresso') ?>">
			</div>

			<div class="form-row">
				<label for="inputEmail">E-mail</label>
				<input type="email" name="email" id="inputEmail" required value="<?php if($this->session->flashdata('email')) echo $this->session->flashdata('email') ?>">
			</div>

			<div class="form-row">
				<label for="inputConfirmeEmail">Confirme seu e-mail</label>
				<input type="email" name="confirmeEmail" id="inputConfirmeEmail" required value="<?php if($this->session->flashdata('confirmeEmail')) echo $this->session->flashdata('confirmeEmail') ?>">
			</div>
			
			<div id="submitPlaceholder">
				<input type="submit" value="RESERVAR INGRESSO" title="Reservar Ingresso">
			</div>

		</div>

	</form>
</div>

<?php if($this->session->flashdata('mensagem')): ?>
	<script defer>
	$('document').ready( function(){
		alert('<?php echo $this->session->flashdata('mensagem')?>');
	});
	</script>
<?php endif; ?>