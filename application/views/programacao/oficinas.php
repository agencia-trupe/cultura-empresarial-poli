<div class="h600">
	<h1>Oficinas</h1>

	<?php if ($oficinas): ?>
		
		<div class="divs-container">

			<?php foreach ($oficinas as $key => $value): ?>

				<div class="oficina">
					
					<div class="skew courtain">
						<h2><?=mb_strtoupper($value->titulo)?> <div class="data"><?=date('d/m', strtotime($value->data))?></div> <div class="seta"></div> </h2>
					</div>
					
					<div class="detalhes-oficina hidden">
						
						<h3>Por <?=$value->por?></h3>

						<div class="texto mini">
							<?=$value->texto?>
						</div>

						<div class="vagas">
							<div class="numero">VAGAS: <span><?=str_pad($value->vagas, 4, '0', STR_PAD_LEFT)?></span></div>
							<a href="programacao/inscricao-oficinas/<?=$value->id?>" title="RESERVAR INGRESSO">RESERVAR INGRESSO <img src="_imgs/layout/icone-ingresso.png" alt="Reserve seu ingresso"></a>
						</div>
	
						<?php if ($value->horario): ?>
							<div class="skew-cinza">
								<div class="horario">
									<span class='label'>Horários:</span><br>
									<?=$value->horario?>
									<?php if ($value->horario_detalhes): ?>
										<small><?=$value->horario_detalhes?></small>
									<?php endif ?>
								</div>
							</div>
						<?php endif ?>

					</div>
					

				</div>

			<?php endforeach ?>

		</div>

	<?php endif ?>
</div>