<div class="h600">
	<h1>Palestrantes</h1>

	<div class="olho"><?=$texto->texto?></div>

	<?php if ($palestrantes): ?>
		
		<div class="container-palest">
			<?php foreach ($palestrantes as $key => $value): ?>
				
				<div class="container">
					<a href="programacao/perfil/<?=$value->id?>" class="fancy-palest" title="<?=$value->nome?>">
						<img src="_imgs/palestrantes/<?=$value->imagem?>" alt="<?=$value->nome?>">
						<div class="texto"><?=$value->nome?><span class="laranja">saiba mais &raquo;</span></div>
					</a>
				</div>

			<?php endforeach ?>
		</div>

	<?php endif ?>
</div>