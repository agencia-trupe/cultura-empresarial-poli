<div class="h600">
	<h1>O Evento</h1>

	<div class="olho"><?=$evento[0]->olho?></div>

	<div class="texto"><?=$evento[0]->texto?></div>
</div>

<div class="galeria">

	<?php if ($evento[0]->imagens): ?>

		<a class="prev" href="#" title="Imagem Anterior"></a>

		<div class="animate">
			<?php foreach ($evento[0]->imagens as $key => $value): ?>
				<img src="_imgs/evento/<?=$value->imagem?>">
			<?php endforeach ?>
		</div>

		<a class="next" href="#" title="Próxima Imagem"></a>

	<?php endif ?>

</div>

<div id="pager"></div>

<div class="obs"><?=$evento[0]->obs?></div>