<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class MY_Frontcontroller extends CI_controller {

    var $headervar;
    var $footervar;
    var $menuvar;
    var $hasLayout;

    function __construct($css = '', $js = '') {
        parent::__construct();
        
        $this->headervar['load_css'] = $css;
        $this->headervar['load_js'] = $js;
        //$this->output->enable_profiler(TRUE);
        $this->hasLayout = TRUE;
        $this->load->model('contato_model','contato');
        $this->load->model('como_chegar_model','comochegar');
    }
    
    function _output($output){

        $this->menuvar['contato'] = $this->contato->pegarTodos();
        $this->footervar['contato'] = $this->menuvar['contato'];
        $this->footervar['como_chegar'] = $this->comochegar->pegarTodos();
        $this->footervar['organizacao']['realizacao'] = $this->db->order_by('ordem', 'asc')->get_where('organizacao', array('categoria'=>'realizacao'))->result();
        $this->footervar['organizacao']['patrocinio'] = $this->db->order_by('ordem', 'asc')->get_where('organizacao', array('categoria'=>'patrocinio'))->result();
        $this->footervar['organizacao']['apoio'] = $this->db->order_by('ordem', 'asc')->get_where('organizacao', array('categoria'=>'apoio'))->result();
        
        if ($this->hasLayout) {        
            echo $this->load->view('common/header', $this->headervar, TRUE).
                 $this->load->view('common/menu', $this->menuvar, TRUE).
                 $output.
                 $this->load->view('common/footer', $this->footervar, TRUE);
        } else {
            echo $output;
        }
        
    }

}
?>