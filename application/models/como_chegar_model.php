<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Como_chegar_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'como_chegar';
		
		$this->dados = array('gmaps', 'endereco', 'texto', 'olho');
		$this->dados_tratados = array(
			'gmaps' => preg_replace('~<br /><small>(.*)</small>~', '', $this->input->post('gmaps'))
		);
	}
}