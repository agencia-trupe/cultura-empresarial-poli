<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Evento_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'evento';
		$this->tabela_imagens = 'evento_imagens';

		$this->dados = array('olho', 'texto', 'obs');
		$this->dados_tratados = array();
	}

	function sobeImagem($campo = 'userfile'){
		$this->load->library('upload');
		ini_set("memory_limit","256M");
		
		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/evento/'
		);
		$campo = $original['campo'];
	
		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0');
	
		$this->upload->initialize($uploadconfig);
	
		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);
	
	        	 $this->image_moo
	        	 		->load($original['dir'].$filename)
	         	  		->resize_crop(838, 380)
	         	  		->save($original['dir'].$filename, TRUE)
	         	  		->resize_crop(165,80)
	         	  		->save($original['dir'].'thumbs/'.$filename, TRUE);
	
		        return $filename;
		    }
		}else{
		    return false;
		}		
	}	
}