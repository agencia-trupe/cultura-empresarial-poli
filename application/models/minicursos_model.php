<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Minicursos_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'mini_cursos';
		$this->tabela_inscricoes = 'mini_cursos_inscricoes';

		$this->dados = array('titulo', 'texto', 'horario', 'horario_detalhes', 'data', 'por', 'n_vagas');
		$this->dados_tratados = array(
			'data' => formataData($this->input->post('data'), 'br2mysql')
		);
	}

	function pegarTodos($order_campo = 'ordem', $order = 'ASC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela)->result();
	}

	function pegarPaginado($por_pagina, $inicio, $order_campo = 'ordem', $order = 'ASC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela, $por_pagina, $inicio)->result();
	}

	function inscricoes($mini_cursos_id, $id_inscricao = false){
		if(!$id_inscricao){
			return $this->db->order_by('data_inscricao', 'DESC')->get_where($this->tabela_inscricoes, array('mini_cursos_id' => $mini_cursos_id))->result();
		}else{
			$query = $this->db->get_where($this->tabela_inscricoes, array('id' => $id_inscricao))->result();
			if(isset($query[0]))
				return $query[0];
			else
				return FALSE;
		}
	}

	function inscrever(){
		$retorno['status'] = false;
		$retorno['mensagem'] = '';
		$retorno['dados'] = null;

		$mini_cursos_id = $this->input->post('mini_cursos_id');
		$nome = $this->input->post('nome');
		$celular = $this->input->post('celular');
		$faculdade = $this->input->post('faculdade');
		$curso = $this->input->post('curso');
		$anoIngresso = $this->input->post('anoIngresso');
		$email = $this->input->post('email');
		$confirmeEmail = $this->input->post('confirmeEmail');

		if(!$mini_cursos_id){ return array('status' => false, 'mensagem' => 'Informe o Mini Curso desejado!'); }
		if(!$nome){ return array('status' => false, 'mensagem' => 'Informe o seu nome!'); }
		if(!$celular){ return array('status' => false, 'mensagem' => 'Informe um celular para contato!'); }
		if(!$faculdade){ return array('status' => false, 'mensagem' => 'Informe a Faculdade!'); }
		if(!$curso){ return array('status' => false, 'mensagem' => 'Informe o curso!'); }
		if(!$anoIngresso){ return array('status' => false, 'mensagem' => 'Informe o ano de ingresso na faculdade!'); }
		if(!$email){ return array('status' => false, 'mensagem' => 'Informe o seu e-mail!'); }
		if(!$confirmeEmail){ return array('status' => false, 'mensagem' => 'Informe a confirmação do seu e-mail! '); }
		if($email != $confirmeEmail){ return array('status' => false, 'mensagem' => 'O e-mail informado não coincide com a confirmação.'); }
		if($this->emailJaCadastrado($email, $mini_cursos_id)){ return array('status' => false, 'mensagem' => 'O e-mail informado já está cadastrado!'); }
		if($this->pegarPorId($mini_cursos_id) === false){ return array('status' => false, 'mensagem' => 'Curso não encontrado!'); }
		if($this->vagas($mini_cursos_id) <= 0){ return array('status' => false, 'mensagem' => 'Não há mais vagas para o curso selecionado!'); }

		$insert = $this->db->set('nome', $nome)
						   ->set('celular', $celular)
						   ->set('faculdade', $faculdade)
						   ->set('curso', $curso)
						   ->set('ano_ingresso', $anoIngresso)
						   ->set('email', $email)
						   ->set('mini_cursos_id', $mini_cursos_id)
						   ->set('data_inscricao', date('Y-m-d H:i:s'))
						   ->set('ip_inscricao', ip())
						   ->insert($this->tabela_inscricoes);

		$this->enviarEmail();

		return array('status' => true, 'mensagem' => 'Inscrição Efetuada!', 'dados' => $this->input->post());
	}

	function enviarEmail(){
		$mini_cursos_id = $this->input->post('mini_cursos_id');
		$nome = $this->input->post('nome');
		$celular = $this->input->post('celular');
		$faculdade = $this->input->post('faculdade');
		$curso = $this->input->post('curso');
		$anoing = $this->input->post('anoIngresso');
		$emailu = $this->input->post('email');
		
        $emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';
        //$emailconf['protocol'] = 'smtp';
        //$emailconf['smtp_host'] = 'smtp.polijr.com.br';
        //$emailconf['smtp_user'] = 'noreply@polijr.com.br';
        //$emailconf['smtp_pass'] = 'no762PoJrply';

        $this->load->library('email');

        $this->email->initialize($emailconf);

        $minicurso = $this->pegarPorId($mini_cursos_id);

        $from = 'noreply@polijr.com.br';
        $fromname = 'SCE Poli Jr';
        $to = $emailu;
        $cc = 'sce@polijr.com.br';
        $bcc = 'bruno@trupe.net';
        $assunto = 'SCE Poli Jr - Inscrição';
        
        
        $emailtxt = <<<EML
<!DOCTYPE html>
<html>
<head>
<title>Mensagem de contato via formulário do site</title>
<meta charset="utf-8">
</head>
<body>
<h1>Confirmação de Inscrição - {$minicurso->titulo}</h1>
<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Mini Curso :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{$minicurso->titulo}</span><br>
<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Nome :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{$nome}</span><br>
<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Celular :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{$celular}</span><br>
<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Faculdade :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{$faculdade}</span><br>
<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Curso :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{$curso}</span><br>
<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Ano de Ingresso :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{$anoing}</span><br>
<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>E-mail :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{$emailu}</span><br>
</body>
</html>
EML;

		$plain = <<<EML
Confirmação de Inscrição - {$minicurso->titulo}\r\n
Mini Curso: {$minicurso->titulo}\r\n
Nome: {$nome}\r\n
Celular: {$celular}\r\n
Faculdade: {$faculdade}\r\n
Curso: {$curso}\r\n
Ano de Ingresso: {$anoing}\r\n
E-mail: {$emailu}\r\n
EML;
		
        $this->email->from($from, $fromname);
        $this->email->to($to);
        if($cc)
            $this->email->cc($cc);
        if($bcc)
            $this->email->bcc($bcc);
        $this->email->reply_to($from);

        $this->email->subject($assunto);
        $this->email->message($emailtxt);
        $this->email->set_alt_message($plain);

        if(!$this->email->send())
        	die($this->email->print_debugger());
    
	}

	function emailJaCadastrado($email, $id_curso){
		return ($this->db->get_where($this->tabela_inscricoes, array('email' => $email, 'mini_cursos_id' => $id_curso))->num_rows() > 0) ? true : false;
	}

	function n_inscricoes($id_curso){
		return $this->db->get_where($this->tabela_inscricoes, array('mini_cursos_id' => $id_curso))->num_rows();
	}

	function vagas($id_minicurso){
		$query = $this->pegarPorId($id_minicurso);
		$n_inscricoes = $this->db->get_where($this->tabela_inscricoes, array('mini_cursos_id' => $id_minicurso))->num_rows();
		$x = $query->n_vagas - $n_inscricoes;
		return ($x > 0) ? $x : 0;
	}

	function excluirInscricao($id){
		return $this->db->where('id', $id)->delete($this->tabela_inscricoes);
	}
	
}