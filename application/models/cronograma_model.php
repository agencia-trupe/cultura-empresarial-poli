<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cronograma_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'cronograma';
		$this->tabela_imagens = 'cronograma_item';

		$this->dados = array('data');
		$this->dados_tratados = array(
			'data' => formataData($this->input->post('data'), 'br2mysql')
		);
	}

	function pegarTodos($order_campo = 'data', $order = 'ASC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela)->result();
	}
	function pegarPaginado($por_pagina, $inicio, $order_campo = 'data', $order = 'ASC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela, $por_pagina, $inicio)->result();
	}

	function inserir(){
		$data_inserir = $this->dados_tratados['data'];
		if($this->db->get_where($this->tabela, array('data' => $data_inserir))->num_rows() == 0){
			return $this->db->set('data', $data_inserir)->insert($this->tabela);
		}else{
			return false;
		}
	}

	function pegarAtividadePorId($id){
		$qry = $this->db->get_where($this->tabela_imagens, array('id' => $id))->result();
		if(isset($qry[0]))
			return $qry[0];
		else
			return FALSE;
	}

	function atividades($id_parent, $id_atividade = FALSE){
		if(!$id_atividade){
			return $this->db->order_by('horario', 'asc')->get_where($this->tabela_imagens, array('id_parent' => $id_parent))->result();
		}else{
			$query = $this->db->get_where($this->tabela_imagens, array('id' => $id_atividade))->result();
			if(isset($query[0]))
				return $query[0];
			else
				return FALSE;
		}
	}

	function inserirAtividade(){

		$palestrante_tipo = $this->input->post('palestrante_tipo');
		$this->db->set('palestrante_tipo', $palestrante_tipo);

		if($palestrante_tipo == 'cadastrado'){

			$this->db->set('palestrante_detalhe', $this->input->post('palestrante_detalhe_id'));

		}elseif($palestrante_tipo == 'texto_livre'){

			$this->db->set('palestrante_detalhe', $this->input->post('palestrante_detalhe_txt'));

		}elseif($palestrante_tipo == 'a_definir'){

			$this->db->set('palestrante_detalhe', 'A Definir');

		}elseif($palestrante_tipo == 'sem_palestrante'){

			$this->db->set('palestrante_detalhe', '');

		}

		$titulo_tipo = $this->input->post('titulo_tipo');
		$this->db->set('titulo_tipo', $titulo_tipo);

		if($titulo_tipo == 'oficina'){

			$this->db->set('titulo_detalhe', $this->input->post('titulo_detalhe_oficina_id'));

		}elseif($titulo_tipo == 'minicurso'){

			$this->db->set('titulo_detalhe', $this->input->post('titulo_detalhe_minicurso_id'));

		}elseif($titulo_tipo == 'texto_livre'){

			$this->db->set('titulo_detalhe', $this->input->post('titulo_detalhe_txt'));

		}

		return $this->db->set('horario', $this->input->post('data_parent').' '.$this->input->post('horario').':00')
						->set('tipo', $this->input->post('tipo'))
						->set('id_parent', $this->input->post('parent_id'))
						->insert($this->tabela_imagens);
	}

	function editarAtividade($id_atividade){

		$palestrante_tipo = $this->input->post('palestrante_tipo');
		$this->db->set('palestrante_tipo', $palestrante_tipo);

		if($palestrante_tipo == 'cadastrado'){

			$this->db->set('palestrante_detalhe', $this->input->post('palestrante_detalhe_id'));

		}elseif($palestrante_tipo == 'texto_livre'){

			$this->db->set('palestrante_detalhe', $this->input->post('palestrante_detalhe_txt'));

		}elseif($palestrante_tipo == 'a_definir'){

			$this->db->set('palestrante_detalhe', 'A Definir');

		}elseif($palestrante_tipo == 'sem_palestrante'){

			$this->db->set('palestrante_detalhe', '');

		}

		$titulo_tipo = $this->input->post('titulo_tipo');
		$this->db->set('titulo_tipo', $titulo_tipo);

		if($titulo_tipo == 'oficina'){

			$this->db->set('titulo_detalhe', $this->input->post('titulo_detalhe_oficina_id'));

		}elseif($titulo_tipo == 'minicurso'){

			$this->db->set('titulo_detalhe', $this->input->post('titulo_detalhe_minicurso_id'));

		}elseif($titulo_tipo == 'texto_livre'){

			$this->db->set('titulo_detalhe', $this->input->post('titulo_detalhe_txt'));

		}

		return $this->db->set('horario', $this->input->post('data_parent').' '.$this->input->post('horario').':00')
						->set('tipo', $this->input->post('tipo'))
						->where('id', $id_atividade)
						->update($this->tabela_imagens);	
	}

	function excluirAtividade($id_atividade){
		return $this->db->delete($this->tabela_imagens, array('id' => $id_atividade));
	}	
}