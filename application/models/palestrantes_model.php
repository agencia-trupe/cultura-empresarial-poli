<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Palestrantes_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'palestrantes';
		//$this->tabela_imagens = 'tabela_imagens'

		$this->dados = array('nome','imagem','texto');
		$this->dados_tratados = array(
			'imagem' => $this->sobeImagem()
		);
	}

	function pegarTodos($order_campo = 'ordem', $order = 'ASC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela)->result();
	}
	function pegarPaginado($por_pagina, $inicio, $order_campo = 'ordem', $order = 'ASC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela, $por_pagina, $inicio)->result();
	}

	function pegarTexto(){
		$query = $this->db->get('palestrante_texto')->result();
		if (isset($query[0])){
			return $query[0];
		}else{
			return false;
		}
	}

	function alterarTexto($id){
		return $this->db->set('texto', $this->input->post('texto'))
					    ->where('id', $id)
				 		->update('palestrante_texto');
	}

	function sobeImagem($campo = 'userfile'){
		$this->load->library('upload');
		ini_set("memory_limit","256M");
		
		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/palestrantes/'
		);
		$campo = $original['campo'];
	
		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0');
	
		$this->upload->initialize($uploadconfig);
	
		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);
	
	        	 $this->image_moo
	        	 		->load($original['dir'].$filename)
	         	  		->resize_crop(313, 257)
	         	  		->save($original['dir'].$filename, TRUE);
	
		        return $filename;
		    }
		}else{
		    return false;
		}		
	}	
}