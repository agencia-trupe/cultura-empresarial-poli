<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ingressos_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'ingressos';
		//$this->tabela_imagens = 'tabela_imagens';

		$this->dados = array('olho', 'texto');
		$this->dados_tratados = array();
	}
}