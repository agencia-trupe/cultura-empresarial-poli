<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Organizacao_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'organizacao';
		//$this->tabela_imagens = 'tabela_imagens'

		$this->dados = array('titulo', 'imagem_fundo_branco', 'imagem_fundo_transparente', 'categoria', 'texto');
		$this->dados_tratados = array(
			'imagem_fundo_branco' => $this->sobeImagem('userfile1'),
			'imagem_fundo_transparente' => $this->sobeImagem('userfile2')
		);
	}

	function pegarTodos($order_campo = 'id', $order = 'ASC', $tipo = 'todos'){
		if ($tipo == 'todos') {
			return $this->db->order_by($order_campo, $order)->get($this->tabela)->result();			
		}else{
			return $this->db->order_by($order_campo, $order)->get_where($this->tabela, array('categoria' => $tipo))->result();			
		}
	}

	function sobeImagem($campo = 'userfile'){
		$this->load->library('upload');
		ini_set("memory_limit","256M");
		
		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/organizacao/'
		);
		$campo = $original['campo'];
	
		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0');
	
		$this->upload->initialize($uploadconfig);
	
		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);
	
	        	 $this->image_moo
	        	 		->load($original['dir'].$filename)
	         	  		->resize(155, 115)
	         	  		->save($original['dir'].$filename);
	
		        return $filename;
		    }
		}else{
		    return false;
		}		
	}	
}