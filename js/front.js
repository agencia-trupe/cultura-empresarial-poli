function str_pad(input, pad_length, pad_string, pad_type) {

  var half = '',
    pad_to_go;

  var str_pad_repeater = function(s, len) {
    var collect = '',
      i;

    while (collect.length < len) {
      collect += s;
    }
    collect = collect.substr(0, len);

    return collect;
  };

  input += '';
  pad_string = pad_string !== undefined ? pad_string : ' ';

  if (pad_type !== 'STR_PAD_LEFT' && pad_type !== 'STR_PAD_RIGHT' && pad_type !== 'STR_PAD_BOTH') {
    pad_type = 'STR_PAD_RIGHT';
  }
  if ((pad_to_go = pad_length - input.length) > 0) {
    if (pad_type === 'STR_PAD_LEFT') {
      input = str_pad_repeater(pad_string, pad_to_go) + input;
    } else if (pad_type === 'STR_PAD_RIGHT') {
      input = input + str_pad_repeater(pad_string, pad_to_go);
    } else if (pad_type === 'STR_PAD_BOTH') {
      half = str_pad_repeater(pad_string, Math.ceil(pad_to_go / 2));
      input = half + input + half;
      input = input.substr(0, pad_length);
    }
  }

  return input;
}

$('document').ready( function(){

	Modernizr.load([
		{
			test: Modernizr.input.placeholder,
		  	nope: 'js/polyfill-placeholder.js'
		}
	]);

	$('#inputAnoIngresso').mask('9999');

	$('#banners').cycle({
		pager:  $('#banners-nav'),
		pause : 1,
		timeout : 6000,
		pagerAnchorBuilder: function(idx, slide) { 
	        return "<div class='container'><a href='#'>"+( idx + 1 )+"</a></div>";
	    }
	});

	$('.main-home .abre-video').fancybox({
		width: 733,
		height: 412,
		type: 'iframe',
		fitToView : false,
		titleShow : false
	});

	$('.main-evento .animate').cycle({
	    fx:     'fade',
	    timeout: 0,
	    pager:  '#pager',
	    prev: '.galeria .prev',
	    next: '.galeria .next',
	    pagerAnchorBuilder: function(idx, slide) { 
	    	console.log(slide.src)
	        return "<a href='#' class='pagerThumb'><img src='" + slide.src.replace('evento/', 'evento/thumbs/') + "'/></a>";
	    } 
	});

	$('.main-acaosocial .animate').cycle({
	    fx:     'fade',
	    timeout: 0,
	    pager:  '#pager',
	    prev: '.galeria .prev',
	    next: '.galeria .next',
	    pagerAnchorBuilder: function(idx, slide) { 
	    	console.log(slide.src)
	        return "<a href='#' class='pagerThumb'><img src='" + slide.src.replace('acao_social/', 'acao_social/thumbs/') + "'/></a>";
	    } 
	});

	$('.main-programacao-oficinas .oficina .skew.courtain h2').click( function(){
		var el = $(this).parent().next('.detalhes-oficina');
		var seta = $(this).find('.seta');
		seta.toggleClass('flipped');
		el.toggleClass('hidden');
	});

	$('.main-programacao-minicursos .oficina .skew.courtain h2').click( function(){
		var el = $(this).parent().next('.detalhes-oficina');
		var seta = $(this).find('.seta');
		seta.toggleClass('flipped');
		el.toggleClass('hidden');
	});	

	$('.fancy-palest, .fancy-ofic, .fancy-organ').fancybox({
		titleShow : false,
		width:720,
		fitToView : false,
		type: 'ajax'
	});

	$('#form-contato').submit( function(){
		if($("input[name='nome']", $(this)).val() == ''){
			alert('Informe seu nome!');
			return false;
		}
		if($("input[name='email']", $(this)).val() == ''){
			alert('Informe seu email!');
			return false;
		}
		if($("textarea[name='mensagem']", $(this)).val() == ''){
			alert('Informe sua mensagem!');
			return false;
		}
	});

	$("#selMiniCursosId").change( function(){
		$('#cursoSel .vagasSel .numero span').html(str_pad($("#selMiniCursosId option:selected").attr('data-vagas'), 4, '0', 'STR_PAD_LEFT'));
	});

	$('#formInscricao').submit( function(e){
		if($("#selMiniCursosId", $(this)).val() == ''){
			alert('Informe o curso desejado!');
			e.preventDefault();
			return false;
		}
		if($("#selMiniCursosId option:selected", $(this)).attr('data-vagas') == '0'){
			alert('Não há mais vagas para o curso selecionado!');
			e.preventDefault();
			return false;
		}
		if($("#selMiniCursosId option:selected", $(this)).attr('data-vagas') == '0'){
			alert('Não há mais vagas para o curso selecionado!');
			e.preventDefault();
			return false;
		}
		if($('#inputNome').val() == ''){
			alert('Informe seu nome!');
			e.preventDefault();
			return false;
		}
		if($('#inputCelular').val() == ''){
			alert('Informe um celular para contato!');
			e.preventDefault();
			return false;
		}
		if($('#inputFaculdade').val() == ''){
			alert('Informe sua Faculdade!');
			e.preventDefault();
			return false;
		}
		if($('#inputCurso').val() == ''){
			alert('Informe seu Curso!');
			e.preventDefault();
			return false;
		}
		if($('#inputAnoIngresso').val() == ''){
			alert('Informe seu Ano de ingresso na Faculdade!');
			e.preventDefault();
			return false;
		}
		if($('#inputEmail').val() == ''){
			alert('Informe seu E-mail!');
			e.preventDefault();
			return false;
		}
		if($('#inputConfirmeEmail').val() == ''){
			alert('Informe uma confirmação para seu E-mail');
			e.preventDefault();
			return false;
		}
		if($('#inputEmail').val() != $('#inputConfirmeEmail').val()){
			alert('O e-mail informado não confere com a confirmação!');
			e.preventDefault();
			return false;
		}
	});

});