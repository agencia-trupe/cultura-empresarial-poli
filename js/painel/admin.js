var Admin = {

  toggleLoginRecovery: function(){
    var is_login_visible = $('#modal-login').is(':visible');
    (is_login_visible ? $('#modal-login') : $('#modal-recovery')).slideUp(300, function(){
      (is_login_visible ? $('#modal-recovery') : $('#modal-login')).slideDown(300, function(){
        $(this).find('input:text:first').focus();
      });
    });
  },

  scrollTop: function(){
    $('html, body').animate({
      'scrollTop' : 0
    }, 300);
  },
   
  voltar: function(){
    window.history.back();
  },

  mostraAlerta: function(){
    var alerta = $('.alert');
    setTimeout( function(){
      alerta.slideUp(400, function(){
        alerta.remove();
      })
    }, 4000);    
  },

  mostraBotaoScroll: function(){
    var hasVScroll = parseInt($('body').css('height')) > window.innerHeight;
    if(!hasVScroll)
      $('#footer .inner .container .right a').hide();
  }


};

$(function(){

  $('.toggle-login-recovery').click(function(e){
    Admin.toggleLoginRecovery();
    e.preventDefault();
  });

  $('#footer .inner .container .right a').click( function(e){
    Admin.scrollTop();
    e.preventDefault();
  });

  $('.btn-move').click( function(e){
    e.preventDefault(); 
  });

  $('.btn-delete').click( function(e){
      var destino = $(this).attr('href');
      bootbox.confirm("Deseja Excluir o Registro?", function(result){
        if(result)
          window.location = destino;
      });
      e.preventDefault();
  });

  $('.btn-voltar').click( function(){
      Admin.voltar();
  });  

  if($('.alert').length){
      Admin.mostraAlerta();
  }

  $("table.table-sortable tbody").sortable({
      update : function () {
          serial = [];
          tabela = $('table.table-sortable').attr('data-tabela');
          $('table.table-sortable tbody').children('tr').each(function(idx, elm) {
              serial.push(elm.id.split('_')[1])
          });
          $.post(BASE+'/ajax/gravaOrdem', { data : serial , tabela : tabela });
      },
      helper: function(e, ui) {
        ui.children().each(function() {
          $(this).width($(this).width());
        });
        return ui;
      },
      handle : $('.btn-move')
  }).disableSelection();

  $('.datepicker').datepicker();

  $('.monthpicker').monthpicker();

  $('.timepicker').timepicker({
    timeOnlyTitle: 'Escolha o Horário',
    timeText: 'Horário',
    hourText: 'Hora',
    minuteText: 'Minuto',
    secondText: 'Segundo',
    currentText: 'Agora',
    closeText: 'Ok'
  });

  Admin.mostraBotaoScroll();

    $('.show.hid').hide();

    if($("input[name=palestrante_tipo]:checked").val() == 'cadastrado'){$('.show.hid.pal.sel').show();}
    if($("input[name=palestrante_tipo]:checked").val() == 'texto_livre'){$('.show.hid.pal.txt').show();}

    if($("input[name=titulo_tipo]:checked").length){$("input[name=titulo_tipo]:checked").next('.show.hid').show();}

    $("input[name=palestrante_tipo]").change( function(){
        $('.show.hid.pal').hide();
        if($(this).val() == 'cadastrado'){
            $(this).next('.show.hid.pal').show();
        }else if($(this).val()=='texto_livre'){
            $(this).next('.show.hid.pal').show();
        }
    });

    $("input[name=titulo_tipo]").change( function(){
        if($(this).val() == 'oficina'){
            $('.show.hid.tit').hide();
            $(this).next(".show.hid[name='titulo_detalhe_oficina_id']").show();
        }else if($(this).val() == 'minicurso'){
            $('.show.hid.tit').hide();
            $(this).next(".show.hid[name='titulo_detalhe_minicurso_id']").show();
        }else{
            $('.show.hid.tit').hide();
            $(this).next(".show.hid[name='titulo_detalhe_txt']").show();
        }
    });

    $('.btn-viewinsc').fancybox();

    $('.fancy-ofic').fancybox({
      type : 'inline',
      autoDimensions : false,
      width : 600,
      height : 300
    });

    $('#searchInscricao').keyup( function(){
      var termo = $(this).val();
      var resultados = '';
      $.post(BASE+'/ajax/buscaInscricao', {termo : termo, tipo : $(this).attr('data-tipo')}, function(resposta){
        resposta = JSON.parse(resposta);
        console.log(resposta)
        for (var i = resposta.length - 1; i >= 0; i--) {
          resultados += "<li><a href='painel/minicursos/inscricoes/"+resposta[i].mini_cursos_id+"/#"+resposta[i].id+"' title='Ver inscrição'>&rarr; "+resposta[i].nome+' (curso: '+resposta[i].nomeCurso+")</a></li>";
        };
        $('#resultados').html(resultados);
      });
    });

    if(window.location.hash){
      setTimeout( function(){
        $('#viewInsc-'+window.location.hash.replace('#','')).click();
      }, 500);
    }

  tinyMCE.init({
      language : "pt",
      mode : "specific_textareas",
      editor_selector : "imagem",
      theme : "advanced",
      theme_advanced_buttons1 : "formatselect,separator,bold,italic,underline,separator,link,unlink,separator,image,fontsizeselect",
      theme_advanced_buttons2 : "tablecontrols,table,row_props,cell_props,delete_col,delete_row,col_after,col_before,row_after,row_before,split_cells,merge_cells",
      theme_advanced_buttons3 : "",
      theme_advanced_blockformats : "h1, h2, p",
      theme_advanced_toolbar_location : "top",
      theme_advanced_statusbar_location : 'bottom',
      theme_advanced_path : false,
      plugins: "paste,advimage,table",
      paste_text_sticky : true,
      theme_advanced_resizing : true,
      setup : function(ed) {
          ed.onInit.add(function(ed) {
              ed.getDoc().body.style.fontSize = '14px';
              ed.pasteAsPlainText = true;
          });
      },
      theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px",
      font_size_style_values : "10px,12px,13px,14px,16px,18px",
      content_css : BASE+"/../css/painel/css/tinymce.css?nocache=" + new Date().getTime()
  });

  tinyMCE.init({
      language : "pt",
      mode : "specific_textareas",
      editor_selector : "completo",
      theme : "advanced",
      theme_advanced_buttons1 : "formatselect,separator,bold,italic,underline,separator,link,unlink,fontsizeselect",
      theme_advanced_buttons2 : "tablecontrols,table,row_props,cell_props,delete_col,delete_row,col_after,col_before,row_after,row_before,split_cells,merge_cells",
      theme_advanced_buttons3 : "",
      theme_advanced_blockformats : "h1, h2, p",
      theme_advanced_toolbar_location : "top",
      theme_advanced_statusbar_location : 'bottom',
      theme_advanced_path : false,
      plugins: "paste,table",
      paste_text_sticky : true,
      theme_advanced_resizing : true,
      setup : function(ed) {
          ed.onInit.add(function(ed) {
              ed.getDoc().body.style.fontSize = '14px';
              ed.pasteAsPlainText = true;
          });
      },
      theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px",
      font_size_style_values : "10px,12px,13px,14px,16px,18px",
      content_css : BASE+"/../css/painel/css/tinymce.css?nocache=" + new Date().getTime()
  });

  tinyMCE.init({
      language : "pt",
      mode : "specific_textareas",
      editor_selector : "basico",
      theme : "advanced",
      theme_advanced_buttons1 : "bold,italic,underline,link,unlink,bullist,fontsizeselect",
      theme_advanced_buttons2 : "tablecontrols,table,row_props,cell_props,delete_col,delete_row,col_after,col_before,row_after,row_before,split_cells,merge_cells",
      theme_advanced_buttons3 : "",
      theme_advanced_toolbar_location : "top",
      theme_advanced_statusbar_location : 'bottom',
      theme_advanced_path : false,
      plugins: "paste,table",
      paste_text_sticky : true,
      theme_advanced_resizing : true,
      setup : function(ed) {
          ed.onInit.add(function(ed) {
              ed.getDoc().body.style.fontSize = '14px';
              ed.pasteAsPlainText = true;
          });
      },
      theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px",
      font_size_style_values : "10px,12px,13px,14px,16px,18px",
      content_css : BASE+"/../css/painel/css/tinymce.css?nocache=" + new Date().getTime()
  });   

});
